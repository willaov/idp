import random
import math
import os
import sys
import argparse
import statistics

# argparse setup
parser = argparse.ArgumentParser()
parser.add_argument(
    "-c", "--clear-path",
    help="clear path for robot",
    action="store_true"
)
parser.add_argument(
    "-d", "--default",
    help="use default block locations",
    action="store_true"
)
parser.add_argument(
    "-t", "--run-tests",
    help="run the set tests",
    action="store_true"
)
parser.add_argument(
    "-r", "--run-random",
    help="run random set of n tests"
)
parser.add_argument(
    "-v", "--visible",
    help="don't minimise tests while running",
    action="store_true"
)
parser.add_argument(
    "-w", "--world",
    help="run a specific world"
)
parser.add_argument(
    "-n", "--no-run",
    help="just generate a world, do not run it",
    action="store_true"
)

# Set this to the world which the random world should be based on
worldFileName = "long_range_test.wbt"

# Set this as the webots executable path
webots = "/Application/Webots.app/webots"

# To give non-random block locations
defaultBlockLocs = [
    (0.377, -0.9),
    (1, 0.3),
    (-1, -0.5),
    (0.1, 1.1),
    (-1.1, 1),
    (0.03, 0.72),
    (-0.85, -0.2),
    (1, 0.7)
]
random1 = [
    (-0.9667835866916972, -0.6353657330199636),
    (1.1569825133670228, -0.014823876026546667),
    (-1.0486800614535305, -0.35446186021932824),
    (-0.08856156120879266, 1.0666926471442189),
    (-0.7881211534595304, 0.6058235748514205),
    (0.1507278325855279, 0.5005654602004157),
    (-1.0752270536780275, -0.6656544564225635),
    (-0.4767658385587463, 0.1030891249959045)
]
random2 = [
    (-0.13955318540963857, 1.007046562502509),
    (0.7480575485269862, 0.24232710660333256),
    (0.6884131504607618, -0.0759572794490957),
    (-0.960707580138883, 0.230579968819012),
    (-0.020271544981713685, -0.9474860203905762),
    (-0.23249336235749607, -0.19157138500228355),
    (-0.022191018513942895, 0.3179879800071501),
    (-1.1449502399321823, 0.3612978540767058)
]
random3 = [
    (0.14097164082432578, -0.5381095626284507),
    (1.0330312173100253, -0.14386592778953622),
    (0.018081241999720277, -0.5895854742616571),
    (-0.09769815532541726, -1.0517616209068097),
    (0.36317601393663734, -0.38853445288444166),
    (-0.3435959387481481, 1.013938214155282),
    (0.11738042036589946, 0.26738231379031974),
    (0.23427350355453136, 0.728833032827785)
]
random4 = [
    (1.1039967756717513, 0.5524887305637476),
    (0.5543759226054699, 0.041577781767097255),
    (0.1705843026146836, -0.2927090831316038),
    (0.5634403665034176, -1.1593894110867937),
    (-0.7540323084231114, -0.2515210702300986),
    (-0.24760626092341798, -1.0468195256993786),
    (0.7515811352996697, -0.3818713266875817),
    (-0.7013754538336998, -0.8212710628878734)
]

tests = [defaultBlockLocs, random1, random2, random3, random4]

blockFile = os.path.join("controllers", "robot_controller", "blocks.csv")
baseWorld = os.path.join("worlds", worldFileName)
randomWorld = os.path.join("worlds", "random_world.wbt")


def randLoc():
    return (random.uniform(-1.165, 1.165), random.uniform(-1.165, 1.165))


def makeValid(randomBlockLocs, clearPath=False):
    valid = False
    while valid is False:
        valid = True
        for i in range(len(randomBlockLocs)):
            # Check for home
            if randomBlockLocs[i][0] > 0.775:
                if randomBlockLocs[i][1] > 0.775 or randomBlockLocs[i][1] < -0.775:
                    valid = False
                    randomBlockLocs[i] = randLoc()
            # Check for intersecting
            for j in range(i):
                x_dist = abs(randomBlockLocs[j][0]-randomBlockLocs[i][0])
                z_dist = abs(randomBlockLocs[j][1]-randomBlockLocs[i][1])
                if x_dist <= 0.05 and z_dist <= 0.05:
                    randomBlockLocs[i] = randLoc()
            if clearPath:
                x = randomBlockLocs[i][0]
                z = randomBlockLocs[i][1]
                d = 0.3
                # Check if at centre
                if -d < x < d:
                    if -d < z < d:
                        print("Moved block from center")
                        valid = False
                        randomBlockLocs[i] = randLoc()
                # Check if along path
                if x > 0 and z > 0:
                    if x < z + d and z < x + d:
                        print("Moved block out of path")
                        valid = False
                        randomBlockLocs[i] = randLoc()


if __name__ == "__main__":
    # Analysis
    failed = 0
    allDists = []
    allDevs = []

    args = parser.parse_args()
    run = 0
    numRuns = 1
    while run < numRuns:
        # Clear previous block data
        fw = open(blockFile, "w")
        fw.close()

        # Read base world file, and get lines to edit
        if args.world:
            baseWorld = os.path.join("worlds", args.world)
        fr = open(baseWorld, "r")
        data = fr.readlines()
        blockLines = []
        for lineNum in range(len(data)):
            if "DEF Block_" in data[lineNum]:
                blockLines.append(lineNum)

        # Parse Arguments and setup block locations
        randomBlockLocs = []
        if args.default:
            randomBlockLocs = defaultBlockLocs
        elif args.run_tests:
            randomBlockLocs = tests[run]
            numRuns = len(tests)
        else:
            if args.run_random:
                numRuns = int(args.run_random)
            for i in range(len(blockLines)):
                randomBlockLocs.append(randLoc())
            makeValid(randomBlockLocs, clearPath=args.clear_path)

        # Write new file with new block locations
        for i in range(len(blockLines)):
            lineNum = blockLines[i]
            data[lineNum+1] = "  translation {} 0.05 {}\n".format(
                randomBlockLocs[i][0], randomBlockLocs[i][1])

        fw = open(randomWorld, "w")
        fw.writelines(data)

        fr.close()
        fw.close()

        if args.no_run:
            break

        for loc in randomBlockLocs:
            print(loc)

        if os.name == "posix":
            cmdArgs = ""
            if args.visible:
                cmdArgs = " --mode=realtime"
            print("Running world automatically")
            if sys.platform == "darwin":
                os.system("./run_random_world.sh /Applications/Webots.app/webots{}".format(cmdArgs))
            else:
                print("Linux is not yet tested")
                os.system("./run_random_world.sh webots{}".format(cmdArgs))
        else:
            print("You're using Windows, so can't run automatically")
            input(
                "Ready to analyse blocks? Press enter once you have run the world.\n" +
                "To run the world from terminal type webots --mode=fast --no-rendering " +
                "--minimize --batch --stdout --stderr " + randomWorld)

        fr = open(blockFile, "r")

        foundBlocks = []
        for i in randomBlockLocs:
            foundBlocks.append(False)

        for block in fr:
            block = block.split(',')
            x_min = float(block[0])
            x_max = float(block[1])
            z_min = float(block[2])
            z_max = float(block[3])
            x = (x_max+x_min)/2
            z = (z_max+z_min)/2
            dev = (x_max-x_min)/2
            if x >= 0.8 and z <= -0.8:
                correct = True
                print("Found Other Robot {:.2f}, {:.2f}".format(x, z))
                print("--------")
            else:
                dists = []
                for loc in randomBlockLocs:
                    dist = math.sqrt((x-loc[0])**2 + (z-loc[1])**2)
                    dists.append(dist)
                minDist = min(dists)
                nearest = randomBlockLocs[dists.index(minDist)]
                foundBlocks[dists.index(minDist)] = True
                allDists.append(minDist)
                allDevs.append(dev)
                print("Found block  {:.2f}, {:.2f},     ±{:.2f}".format(
                    x, z, dev))
                print("Actual block {:.2f}, {:.2f}, dist {:.2f}".format(
                    nearest[0], nearest[1], minDist))
                print("--------")

        for i in range(len(randomBlockLocs)):
            if not foundBlocks[i]:
                failed += 1
                print("Did not find {:.2f}, {:.2f}".format(
                    randomBlockLocs[i][0], randomBlockLocs[i][1]))

        # Increment the run counter
        run += 1

    # Print analysis
    print("All out of range blocks")
    for i in range(len(allDists)):
        if allDevs[i] < allDists[i]:
            print("Out of range: estimated {:.2f}, actual {:.2f}".format(allDevs[i], allDists[i]))
    meanDist = statistics.mean(allDists)
    stdDevDists = statistics.stdev(allDists)
    print("Distance Error μ={:.2f}, σ={:.3f}".format(meanDist, stdDevDists))
    allDiffs = []
    for i in range(len(allDists)):
        allDiffs.append(allDists[i]-allDevs[i])
    meanDiff = statistics.mean(allDiffs)
    stdDevDiffs = statistics.stdev(allDiffs)
    print("Difference μ={:.2f}, σ={:.3f}".format(meanDiff, stdDevDiffs))
    print("Failed to find {} blocks".format(failed))
