mac="/Applications/Webots.app/webots"
default_args="--mode=fast --no-rendering --minimize --batch"
webots="${1:-$mac}"
args="${2:-$default_args}"
command="$webots $args --stdout --stderr worlds/random_world.wbt"
log="random.log"
match="From control to REDBOT, STOP"

$command > "$log" 2>&1 &
pid=$!

while sleep 5
do
    if fgrep --quiet "$match" "$log"
    then
        kill $pid
        exit 0
    fi
done
