import struct
from enum import IntEnum

TIME_STEP = 64
COMMS_CHANNEL = 1


# Comms Schema
class ID(IntEnum):
    BLUEBOT = 0b01
    REDBOT = 0b10
    CONTROL = 0b11


class CMD(IntEnum):
    CONTINUE = 0b0000
    GOTO_AND_SPIN = 0b0001
    GOTO_AND_CHECK = 0b0010
    GOTO_AND_COLLECT = 0b0011
    RETURN_AND_DROP = 0b0100
    GOTO = 0b0101
    STOP = 0b0110
    CLEAR_SPACE = 0b0111
    RED_BLOCK_FOUND = 0b1000
    BLUE_BLOCK_FOUND = 0b1001
    MY_LOC = 0b1010
    SPIN_COMPLETE = 0b1011
    BLOCK_FOUND = 0b1100
    BLOCK_COLLECTED = 0b1101
    BLOCK_DROPPED = 0b1110
    NO_BLOCK = 0b1111


def RECEIVER_MASK(byte):
    return ((byte) & 0b11000000) >> 6


def SENDER_MASK(byte):
    return ((byte) & 0b00110000) >> 4


def COMMAND_MASK(byte):
    return ((byte) & 0b00001111)


def send_packet(emitter, recipient, command, data):
    """Send packet of data

    Parameters:
        emitter (Emitter): The emitter device
        recipient (ID): The recipient ID
        command (CMD): The command to send
        data (list[float]): The data to send

    Returns:
        none
    """
    # Check types of arguments
    try:
        if recipient in ID:
            pass
    except TypeError:
        raise TypeError("Recipient must be a member of the ID enum")
    try:
        if command in CMD:
            pass
    except TypeError:
        raise TypeError("Command must be a member of the CMD enum")
    try:
        assert type(data[0]) is float
    except TypeError:
        raise TypeError("Data must be a list of floats")
    except AssertionError:
        for i in range(len(data)):
            data[i] = float(data[i])
    header = recipient << 6 | ID.CONTROL << 4 | command
    packet = bytearray()
    packet.append(header)
    for thing in data:
        packet.extend(struct.pack('d', thing))
    emitter.send(bytes(packet))


def get_received_packets(receiver):
    """Get all the packets which have been received

    Parameters:
        receiver (Receiver): The receiver device

    Returns:
        list[packet]:  A list of received packets,
            each packet is a tuple(sender(ID), command(CMD), data(tuple(float)))
    """
    packets = []
    while receiver.getQueueLength() > 0:
        packet = receiver.getData()
        recipient = ID(RECEIVER_MASK(packet[0]))
        sender = ID(SENDER_MASK(packet[0]))
        command = CMD(COMMAND_MASK(packet[0]))
        data = ()
        if recipient == ID.CONTROL:
            double_len = struct.calcsize('d')
            data_len = (len(packet)-1)/double_len
            struct_format = "d" * int(data_len)
            data = struct.unpack(struct_format, packet[1:])
        receiver.nextPacket()
        packets.append((sender, command, data))
    return packets


def init_comms_devices(robot, time_step):
    """Initialise Comms Devices

    Parameters:
        robot (Robot): The robot which has the devices
        time_step (int): The time step to use for the comms

    Returns:
        emitter (Emitter), receiver (Receiver): The emitter and receiver devices
    """
    emitter = robot.getDevice("emitter")
    receiver = robot.getDevice("receiver")
    receiver.enable(time_step)
    receiver.setChannel(COMMS_CHANNEL)
    emitter.setChannel(COMMS_CHANNEL)
    return emitter, receiver


if __name__ == "__main__":
    # Main loop:
    from controller import Robot
    import json
    robot = Robot()
    TIME_STEP = 64

    emitter, receiver = init_comms_devices(robot, TIME_STEP)
    keyboard = robot.getKeyboard()
    keyboard.enable(TIME_STEP)

    send = True
    recipient = ID.BLUEBOT
    cmd = CMD.GOTO
    data = [-0.5, 0.5, 0.0, 0.0]
    commandNum = 0
    keyboardWait = 0
    while robot.step(TIME_STEP) != -1:
        packets = get_received_packets(receiver)
        for packet in packets:
            if CMD(packet[1]) != CMD.MY_LOC:
                print("From {} to control, {}: {}".format(packet[0].name, packet[1].name, packet[2]))
            if ID(packet[0]) == recipient and CMD(packet[1]) == CMD.MY_LOC:
                if round(data[0], 1) == round(packet[2][0], 1) and\
                        round(data[1], 1) == round(packet[2][1], 1) and\
                        packet[2][3] < 0.001 and cmd == CMD.GOTO:
                    print("{} has arrived".format(recipient.name))
                    commandNum += 1
                    send = True
            elif ID(packet[0]) == recipient and CMD(packet[1]) == CMD.SPIN_COMPLETE:
                if cmd == CMD.GOTO_AND_SPIN:
                    print("{} has finished spinning".format(recipient.name))
                    commandNum += 1
                    send = True
            elif ID(packet[0]) == recipient and CMD(packet[1]) == CMD.RED_BLOCK_FOUND:
                if cmd == CMD.GOTO_AND_CHECK:
                    print("{} has found a red block".format(recipient.name))
                    commandNum += 1
                    send = True
            elif ID(packet[0]) == recipient and CMD(packet[1]) == CMD.BLUE_BLOCK_FOUND:
                if cmd == CMD.GOTO_AND_CHECK:
                    print("{} has found a blue block".format(recipient.name))
                    commandNum += 1
                    send = True
            elif ID(packet[0]) == recipient and CMD(packet[1]) == CMD.BLOCK_COLLECTED:
                if cmd == CMD.GOTO_AND_COLLECT:
                    print("{} has picked up a block".format(recipient.name))
                    commandNum += 1
                    send = True
            elif ID(packet[0]) == recipient and CMD(packet[1]) == CMD.BLOCK_DROPPED:
                if cmd == CMD.RETURN_AND_DROP:
                    print("{} has deposited a block".format(recipient.name))
                    commandNum += 1
                    send = True
        if keyboardWait == 0:
            while True:
                key = keyboard.getKey()
                if key < 0:
                    break
                elif key == ord('N'):
                    commandNum += 1
                    send = True
                    keyboardWait += 10
        else:
            keyboardWait -= 1
        json_data = json.load(open("test_command.json"))
        if commandNum >= len(json_data):
            commandNum = 0
        packet_to_send = json_data[commandNum]
        recipient = ID[packet_to_send["id"]]
        cmd = CMD[packet_to_send["cmd"]]
        data = packet_to_send["data"]

        if send:
            send = False
            print("From control to {}, {}: {}".format(recipient.name, cmd.name, data))
            send_packet(emitter, recipient, cmd, data)
