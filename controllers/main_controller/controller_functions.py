import numpy as np

#finds minimum distance from a point to a line segment, with vector algebra
def min_distance(a,b,c):
    a=np.asarray(a)
    b=np.asarray(b)
    c=np.asarray(c)
    if np.dot(b-a,c-a)<0:
        d=np.linalg.norm(c-a)
    elif np.dot(a-b,c-b)<0:
        d=np.linalg.norm(c-b)
    else:
        d=abs(np.cross(b-a,c-a)/np.linalg.norm(b-a))
    return d.item()

#P = external point, O = centre of circle, uses np arrays
#function for the equation of a line, for a circle's tangents that intersect an external point
def tan_line(P,O,radius):
    PO=np.asarray(O)-np.asarray(P)
    e_x=np.zeros((2,1))
    e_x[0]=1
    e_x[1]=0
    phi=np.arcsin(radius/np.linalg.norm(PO))
    theta=np.arctan(PO[1]/PO[0])
    slope_1=float(np.tan(theta+phi))
    c_1=float(P[1]-slope_1*P[0])
    slope_2=float(np.tan(theta-phi))
    c_2=float(P[1]-slope_2*P[0])
    return np.array([[slope_1,c_1],[slope_2,c_2]])


#argument must be: (distance, A, B, c [obstacle], d [obstacle], ...)
#onle considers one-turn paths
def path(*args):
    
    #convert inputs to numpy arrays
    min_acc_dist=args[0]
    start=np.asarray(args[1])
    end=np.asarray(args[2])
    point_obstacles=np.asarray(args[3:])
    
    #check if inputs are valid
    if any (np.linalg.norm(i-end)<min_acc_dist for i in point_obstacles) and any (np.linalg.norm(i-start)<min_acc_dist for i in point_obstacles):
        print ("Start and endpoint are too close to a block")
        return
    elif any (np.linalg.norm(i-end)<min_acc_dist for i in point_obstacles):
        print ("Endpoint is too close to a block")
        return
    elif any (np.linalg.norm(i-start)<min_acc_dist for i in point_obstacles):
        print ("Start is too close to a block")
        return
    
    #if straight path from start to finish exists: take it
    if all (min_distance(start,end,i)>=min_acc_dist for i in point_obstacles):
        return start.tolist(),end.tolist()
    
    #finding set of tangent lines from the starting point
    paired_start_lines=np.zeros((len(point_obstacles),2,2))
    for i in range (len(point_obstacles)):
        paired_start_lines[i]=tan_line(start,point_obstacles[i],min_acc_dist)
    #neatening the array
    start_lines=np.zeros((2*len(point_obstacles),2))
    for i in range (2):
        for j in range(len(point_obstacles)):
            start_lines[j+i*len(point_obstacles), :]=paired_start_lines[j,i]
    
    #finding set of tangent lines from the end point
    paired_end_lines=np.zeros((len(point_obstacles),2,2))
    for i in range (len(point_obstacles)):
        paired_end_lines[i]=tan_line(end,point_obstacles[i],min_acc_dist)
    #neatening the array
    end_lines=np.zeros((2*len(point_obstacles),2))
    for i in range (2):
        for j in range(len(point_obstacles)):
            end_lines[j+i*len(point_obstacles), :]=paired_end_lines[j,i]
    
    #finding set of all intersections between start_lines and end_lines
    intersections=np.zeros((2*len(point_obstacles),2*len(point_obstacles),2))
    for i in range (2*len(point_obstacles)):
        for j in range (2*len(point_obstacles)):
            intersections[i,j,0]=-(end_lines[j,1]-start_lines[i,1])/(end_lines[j,0]-start_lines[i,0])
            intersections[i,j,1]=start_lines[i,0]*intersections[i,j,0]+start_lines[i,1]
    
    #disqualifying some intersections
    valid_intersections=intersections
    for i in range (2*len(point_obstacles)):
        for j in range (2*len(point_obstacles)):
            min_distances=np.zeros(2*len(point_obstacles))
            for k in range (len(point_obstacles)):
                min_distances[k]=min_distance(start,intersections[i,j],point_obstacles[k])
                min_distances[k+len(point_obstacles)]=min_distance(end,intersections[i,j],point_obstacles[k])
            #disqualifying intersections that pass through other circles
            if np.amin(min_distances)<0.99*min_acc_dist:
                valid_intersections[i,j]=[100,100]
            #disqualifying intersections that are too close to the edge/outside the arena
            #arena length/2 = 1.19, maximum robot diameter = 0.32
            elif max(abs(valid_intersections[i,j,0]),abs(valid_intersections[i,j,1]))>1.19-(0.32/2):
                valid_intersections[i,j]=[100,100]

    #finding path lengths from start -> intersection -> end
    path_lengths=np.zeros((2*len(point_obstacles),2*len(point_obstacles)))
    for i in range (2*len(point_obstacles)):
        for j in range (2*len(point_obstacles)):
            path_lengths[i][j]=np.linalg.norm(start-valid_intersections[i][j])+np.linalg.norm(end-valid_intersections[i][j])

    #turning point = intersection corresponding to shortest path length
    turning_point=valid_intersections[np.unravel_index(np.argmin(path_lengths, axis=None), path_lengths.shape)]
    #if all intersections have been disqualified, no path found
    if np.linalg.norm(turning_point)>100:
        print ("No path found")
        return
    #else, return (start, turning point, end) as lists
    else:
        return start.tolist(),turning_point.tolist(),end.tolist()
