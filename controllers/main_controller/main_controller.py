try:
    from controller import Robot
except ImportError:
    print("Failed to import from controller, fine if this isn't actually running")
try:
    from .comms import init_comms_devices, send_packet, get_received_packets, CMD, ID
    from .controller_functions import min_distance, path
except (ImportError, ValueError):
    from comms import init_comms_devices, send_packet, get_received_packets, CMD, ID
    from controller_functions import min_distance, path
try:
    import matplotlib.pyplot as plt
    plotting = False
except ImportError:
    plotting = False
import math
import logging
import sys
from enum import IntEnum

root = logging.getLogger()
root.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('Controller:%(levelname)s: %(message)s')
handler.setFormatter(formatter)
warningHandler = logging.StreamHandler(sys.stderr)
warningHandler.setLevel(logging.WARNING)
root.addHandler(handler)
root.addHandler(warningHandler)

ROBOT_WIDTH = 0.3
ROBOT_FRONT_LENGTH = 0.15
ROBOT_REAR_LENGTH = 0.15
ROBOT_DIAG = 0.18
BLOCK_WIDTH = 0.05
IDEAL_BOT_SEPARATION = 0.6
NUM_BLOCKS = 4
HOME_TOLERANCE = 0.05
ALREADY_HERE_TOLERANCE = 0.1
CENTRE_LINE_TOLERANCE = 0.03
STOPPED_BOT_TIMEOUT = 10.0
STOPPED_SPEED_THRESHOLD = 0.001
GIVE_UP_TIMEOUT = 60.0

ARRIVAL_OFFSET = 0.3

# Bluebot won't do anything
NO_BLUEBOT = False

WALL_DIST = 1.19
RED_BASE_LOC = [1.0, 1.0]
BLUE_BASE_LOC = [1.0, -1.0]
RED_HOME_RECTANGLE = [
    (0.8, 0.8),
    (0.8, 1.2),
    (1.2, 1.2),
    (1.2, 0.8)
]
BLUE_HOME_RECTANGLE = [
    (0.8, -0.8),
    (0.8, -1.2),
    (1.2, -1.2),
    (1.2, -0.8)
]
TIME_STEP = 16

EAST_REGION = [
    (1.2, 0.0),
    (1.2, 1.2),
    (-1.2, 1.2),
    (-1.2, 0)
]
WEST_REGION = [
    (1.2, 0.0),
    (1.2, -1.2),
    (-1.2, -1.2),
    (-1.2, 0)
]
NORTH_REGION = [
    (0.0, -1.2),
    (1.2, -1.2),
    (1.2, 1.2),
    (0.0, 1.2)
]
SOUTH_REGION = [
    (0.0, -1.2),
    (-1.2, -1.2),
    (-1.2, 1.2),
    (0.0, 1.2)
]
WHOLE_REGION = [
    (1.2, 1.2),
    (1.2, -1.2),
    (-1.2, -1.2),
    (-1.2, 1.2)
]
SW_SWITCH = [-0.5, -0.2]
SW_SWITCH1 = [-0.75, -0.2]
SW_SWITCH2 = [-1.0, -0.2]
SW_SWITCH3 = [-0.25, -0.2]
SW_SWITCH_OPTS = [SW_SWITCH, SW_SWITCH1, SW_SWITCH2, SW_SWITCH3]
SE_SWITCH = [-0.5, 0.2]
SE_SWITCH1 = [-0.75, 0.2]
SE_SWITCH2 = [-1.0, 0.2]
SE_SWITCH3 = [-0.25, 0.2]
SE_SWITCH_OPTS = [SE_SWITCH, SE_SWITCH1, SE_SWITCH2, SE_SWITCH3]
NE_SWITCH = [0.5, 0.2]
NE_SWITCH1 = [0.75, 0.2]
NE_SWITCH2 = [1.0, 0.2]
NE_SWITCH3 = [0.25, 0.2]
NE_SWITCH_OPTS = [NE_SWITCH, NE_SWITCH1, NE_SWITCH2, NE_SWITCH3]
NW_SWITCH = [0.5, -0.2]
NW_SWITCH1 = [0.75, -0.2]
NW_SWITCH2 = [1.0, -0.2]
NW_SWITCH3 = [0.25, -0.2]
NW_SWITCH_OPTS = [NW_SWITCH, NW_SWITCH1, NW_SWITCH2, NW_SWITCH3]

DIST_FACT = 0.05
CENTRAL_SCAN = [0.0, 0.0, 0.0, 0.0]
SE_SCAN = [0.0, 0.5, 0.0, 0.0]
SW_SCAN = [0.0, -0.5, 0.0, 0.0]

RETREAT_TIME = 280.0
END_TIME = 300.0

# Global Variables
simulationTime = 0


class COLOUR(IntEnum):
    UNKNOWN = 0
    BLUE = 1
    RED = 2
    CHECKING = 3


class STATE(IntEnum):
    FOUND = 0
    MOVING = 1
    HOME = 2


class WEIGHTS(IntEnum):
    EXTRA_SUPER_MAX = 40
    SUPER_MAX = 20
    MAX = 10
    MID = 5
    NEGATIVE = -20


class ANGLE(IntEnum):
    NORTH = 0
    EAST = 90
    SOUTH = 180
    WEST = 270


class Block():
    # Representation fo a block, and methods for getting/modifying blocks
    allBlocks = []

    def setColour(location, colour):
        # Set the colour of a block
        blockDiag = math.sqrt(pow(0.05, 2)*2)/2
        for block in Block.allBlocks:
            dist = math.sqrt(
                pow(block.location[0] - location[0], 2)
                + pow(block.location[1] - location[1], 2))
            if dist < blockDiag:
                if block.colour == COLOUR.UNKNOWN or block.colour == COLOUR.CHECKING:
                    block.colour = colour
                    logging.warning("Block found at ({:.2f}, {:.2f}) with colour {}".format(block.location[0], block.location[1], block.colour.name))
                elif block.colour == colour:
                    pass
                else:
                    logging.error("Block is already assigned colour {}, not {}".format(
                        block.colour.name, colour.name
                    ))

    def setState(location, state):
        # Set the state of a block
        blockDiag = math.sqrt(pow(0.05, 2)*2)/2
        for block in Block.allBlocks:
            dist = math.sqrt(
                pow(block.location[0] - location[0], 2)
                + pow(block.location[1] - location[1], 2))
            if dist < blockDiag:
                block.state = state

    def getBlock(location):
        # Get the block at location
        blockDiag = math.sqrt(pow(0.05, 2)*2)/2
        for block in Block.allBlocks:
            dist = math.sqrt(
                pow(block.location[0] - location[0], 2)
                + pow(block.location[1] - location[1], 2))
            if dist < blockDiag:
                return block

    def colourComplete(colour):
        # Check if all blocks of a colour are collected
        num = 0
        for block in Block.allBlocks:
            if block.colour == colour:
                num += 1
                if block.state != STATE.HOME:
                    # Colour not back at base
                    return False
        if num < NUM_BLOCKS:
            # Still need to find more blocks
            return False
        return True

    def removeBlock(location):
        # Remove block at location, used if a false block was detected/block accidentaly moved
        blockDiag = math.sqrt(pow(0.05, 2)*2)/2
        for block in Block.allBlocks:
            dist = math.sqrt(
                pow(block.location[0] - location[0], 2)
                + pow(block.location[1] - location[1], 2))
            if dist < blockDiag and block.state == STATE.FOUND and block.colour != COLOUR.UNKNOWN:
                logging.warning("Removed block at {:.2f}, {:.2f}".format(block.x, block.z))
                Block.allBlocks.remove(block)

    def plotAll():
        # Plot the block locations on a graph
        x = []
        z = []
        colours = []
        sizes = []
        for block in Block.allBlocks:
            if block.state != STATE.MOVING:
                x.append(block.location[0])
                z.append(-block.location[1])
                sizes.append(((block.uncertainty+BLOCK_WIDTH)*100)**2)
                if block.colour == COLOUR.RED:
                    colours.append('r')
                elif block.colour == COLOUR.BLUE:
                    colours.append('b')
                else:
                    colours.append('tab:grey')
        plt.clf()
        plt.scatter(x, z, s=sizes, c=colours, marker="s")
        plt.axis([-1.2, 1.2, -1.2, 1.2])
        plt.draw()
        plt.pause(0.000001)

    def __init__(self, location, uncertainty):
        # Create a new block, or improve the location accuracy of an existing block
        newBlock = True
        for block in Block.allBlocks:
            dist = math.sqrt(
                pow(block.location[0] - location[0], 2)
                + pow(block.location[1] - location[1], 2))
            if dist < block.uncertainty + uncertainty:
                newBlock = False
                if uncertainty < block.uncertainty and block.colour == COLOUR.UNKNOWN:
                    logging.debug("Found same block with less uncertainty")
                    self.colour = block.colour
                    self.state = block.state
                    Block.allBlocks[Block.allBlocks.index(block)] = self
                    break
                else:
                    logging.debug("Found same block with more uncertainty")
                    break
        self.location = location
        self.x = location[0]
        self.z = location[1]
        self.uncertainty = uncertainty
        if newBlock:
            self.colour = COLOUR.UNKNOWN
            self.state = STATE.FOUND
        if newBlock:
            Block.allBlocks.append(self)

    def move(self, location, uncertainty):
        # Move a block to new location
        self.location = location
        self.x = location[0]
        self.z = location[1]
        self.uncertainty = uncertainty

    def inRectangle(self, rectanglePoints):
        # Check if block is inside a rectangle
        xMax = -100
        zMax = -100
        xMin = 100
        zMin = 100
        for point in rectanglePoints:
            if point[0] > xMax:
                xMax = point[0]
            elif point[0] < xMin:
                xMin = point[0]
            if point[1] > zMax:
                zMax = point[1]
            elif point[1] < zMin:
                zMin = point[1]
        if xMin - BLOCK_WIDTH/2 < self.x < xMax + BLOCK_WIDTH/2\
                and zMin - BLOCK_WIDTH/2 < self.z < zMax + BLOCK_WIDTH/2:
            return True
        return False

    def approachAngle(self, startLoc, region=None):
        # Get the best accessible approach angle to a block, when approaching from startLoc, and staying within region
        xDist = self.x - startLoc[0]
        zDist = self.z - startLoc[1]
        angleOrder = [-1, -1, -1, -1]
        xOrder = [ANGLE.NORTH, ANGLE.SOUTH] if xDist > 0 else [ANGLE.SOUTH, ANGLE.NORTH]
        zOrder = [ANGLE.EAST, ANGLE.WEST] if zDist > 0 else [ANGLE.WEST, ANGLE.EAST]
        if abs(zDist) > abs(xDist):
            angleOrder[::len(angleOrder)-1] = zOrder
            angleOrder[1:3] = xOrder
        else:
            angleOrder[::len(angleOrder)-1] = xOrder
            angleOrder[1:3] = zOrder
        for angle in angleOrder:
            # Check proximity to wall, including diagonal as turning into position
            if angle == ANGLE.SOUTH:
                if WALL_DIST-self.x < ARRIVAL_OFFSET+ROBOT_DIAG:
                    continue
            elif angle == ANGLE.NORTH:
                if WALL_DIST+self.x < ARRIVAL_OFFSET+ROBOT_DIAG:
                    continue
            elif angle == ANGLE.WEST:
                if WALL_DIST-self.z < ARRIVAL_OFFSET+ROBOT_DIAG:
                    continue
            elif angle == ANGLE.EAST:
                if WALL_DIST+self.z < ARRIVAL_OFFSET+ROBOT_DIAG:
                    continue

            approachBox = []
            # Now check the approach box is empty
            if abs(self.z) < CENTRE_LINE_TOLERANCE:
                # If on centreline, then shrink approach box slightly to allow collection
                widthFact = 4.0
            else:
                widthFact = 2.0
            if angle in (ANGLE.NORTH, ANGLE.SOUTH):
                if angle == ANGLE.SOUTH:
                    corner1 = (self.x - BLOCK_WIDTH/widthFact, self.z + ROBOT_DIAG)
                    corner2 = (self.x - BLOCK_WIDTH/widthFact, self.z - ROBOT_DIAG)
                    corner3 = (self.x + (ARRIVAL_OFFSET+ROBOT_DIAG), self.z + ROBOT_DIAG)
                    corner4 = (self.x + (ARRIVAL_OFFSET+ROBOT_DIAG), self.z - ROBOT_DIAG)
                else:
                    corner1 = (self.x + BLOCK_WIDTH/widthFact, self.z + ROBOT_DIAG)
                    corner2 = (self.x + BLOCK_WIDTH/widthFact, self.z - ROBOT_DIAG)
                    corner3 = (self.x - (ARRIVAL_OFFSET+ROBOT_DIAG), self.z + ROBOT_DIAG)
                    corner4 = (self.x - (ARRIVAL_OFFSET+ROBOT_DIAG), self.z - ROBOT_DIAG)
                approachBox = [corner1, corner2, corner3, corner4]
            else:
                if angle == ANGLE.WEST:
                    corner1 = (self.x + ROBOT_DIAG, self.z - BLOCK_WIDTH/widthFact)
                    corner2 = (self.x - ROBOT_DIAG, self.z - BLOCK_WIDTH/widthFact)
                    corner3 = (self.x + ROBOT_DIAG, self.z + (ARRIVAL_OFFSET+ROBOT_DIAG))
                    corner4 = (self.x - ROBOT_DIAG, self.z + (ARRIVAL_OFFSET+ROBOT_DIAG))
                else:
                    corner1 = (self.x + ROBOT_DIAG, self.z + BLOCK_WIDTH/widthFact)
                    corner2 = (self.x - ROBOT_DIAG, self.z + BLOCK_WIDTH/widthFact)
                    corner3 = (self.x + ROBOT_DIAG, self.z - (ARRIVAL_OFFSET+ROBOT_DIAG))
                    corner4 = (self.x - ROBOT_DIAG, self.z - (ARRIVAL_OFFSET+ROBOT_DIAG))
                approachBox = [corner1, corner2, corner3, corner4]
            # First check if any points are outside walls or the region
            invalid = False
            for corner in approachBox:
                if checkOutsideWall(corner):
                    invalid = True
                if region is not None:
                    if not checkInRectangle(region, corner):
                        invalid = True
            if invalid:
                continue
            # Then check if any blocks in the area
            for block in Block.allBlocks:
                if block == self:
                    continue
                if block.inRectangle(approachBox):
                    invalid = True
            if invalid:
                continue
            return angle
        # If no angle accessible then return -1
        return -1

    def approachLocation(self, startLoc, region=None):
        # Get the approach location of a block
        angle = self.approachAngle(startLoc, region)
        if angle >= 0:
            x = self.x - ARRIVAL_OFFSET*cosd(angle)
            z = self.z - ARRIVAL_OFFSET*sind(angle)
            return (x, z)
        elif region is not None and False:
            angle = self.approachAngle(startLoc)
            if angle >= 0:
                x = self.x - ARRIVAL_OFFSET*cosd(angle)
                z = self.z - ARRIVAL_OFFSET*sind(angle)
                if checkInRectangle(region, (x, z)):
                    return (x, z)
                else:
                    return -1
            else:
                return -1
        else:
            return -1

    def __repr__(self):
        return "Block: ({:.2f}, {:.2f}), {:.2f}, {}, {};".format(
            self.location[0], self.location[1], self.uncertainty, self.colour.name, self.state.name)


class Path():
    # Primitive way to check if clear path to location
    def __init__(self, startLoc, endLoc):
        self.startLoc = startLoc
        self.endLoc = endLoc

    @property
    def clear(self):
        # Check if the path is clear
        clear = True
        for block in Block.allBlocks:
            if block.location == self.endLoc:
                # Skip if the block is the end block
                continue
            if block.approachLocation(self.startLoc) == self.endLoc:
                # Skip if the block is the end block
                continue
            if block.state in (STATE.HOME, STATE.MOVING):
                # Skip if the block is at home or moving
                continue
            dist = min_distance(self.startLoc, self.endLoc, block.location)
            if dist < ROBOT_WIDTH/2 + BLOCK_WIDTH:
                logging.debug("Dist {} is too close to {} from {} to {}".format(
                    dist, block.location, self.startLoc, self.endLoc))
                clear = False
        return clear

    def __repr__(self):
        return "Start: ({:.2f},{:.2f}) End ({:.2f},{:.2f})".format(
            self.startLoc[0], self.startLoc[1], self.endLoc[0], self.endLoc[1])


class Route():
    # Route for robot to travel, with command at end
    def __init__(self, points, endCmd=CMD.GOTO, region=None, force=False):
        self._points = points
        self._cmds = [endCmd]
        self._index = 1
        self.new = True
        self.noRoute = False

        if not force:
            avoidPoints = []
            for block in Block.allBlocks:
                if block.state == STATE.FOUND:
                    # Create list of blocks to avoid
                    avoidPoints.append(block.location)
            avoidDist = ROBOT_DIAG + BLOCK_WIDTH
            pathArgs = [avoidDist] + [self._points[0]] + [self._points[-1]] + avoidPoints
            # Now get the route avoiding the blocks
            try:
                try:
                    start, points, end = path(*pathArgs)
                    if type(points[0]) == list:
                        for point in points:
                            self._points.insert(-1, point)
                    else:
                        if not math.isnan(points[0]) and not math.isnan(points[1]):
                            self._points.insert(-1, points)
                except ValueError:
                    start, end = path(*pathArgs)
            except TypeError:
                self.noRoute = True

            # Insert GOTOs into the list to make up the length
            while len(self._cmds) < len(self._points):
                self._cmds.insert(0, CMD.GOTO)

            if self.noRoute:
                return

            # Check if any nan points, in which case there is no route
            for point in points:
                if type(point) in (list, tuple):
                    if math.isnan(point[0]) or math.isnan(point[1]):
                        self.noRoute = True
                else:
                    if math.isnan(point):
                        self.noRoute = True

            # Check for any points outside the region, in which case the route is not valid
            if region is not None:
                for point in self._points[1:]:
                    if not checkInRectangle(region, point):
                        self.noRoute = True
        else:
            # Still make sure cmds is correct length for forced routes
            logging.warning("Forcing route creation, may have collisions")
            while len(self._cmds) < len(self._points):
                self._cmds.insert(0, CMD.GOTO)

    def getNext(self):
        # Increment the route, and returns whether the route is complete
        if self.new:
            self.new = False
            return True
        self._index += 1
        if self._index >= len(self._points):
            return False
        else:
            return True

    def insert(self, start, points):
        # Insert a point into the route
        startNum = 0
        for point in points:
            if point in self._points:
                points.remove(point)
        numPoints = len(points)
        for i in range(len(self._points)):
            if self._points[i] == start:
                startNum = i
                break
        for i in range(numPoints):
            self._points.insert(i + startNum, points[i])
        # Insert GOTOs into the list to make up the length
        while len(self._cmds) < len(self._points):
            self._cmds.insert(0, CMD.GOTO)

    @property
    def point(self):
        # Current point on route
        self.new = False
        return self._points[self._index]

    @property
    def prevPoint(self):
        # Previous point on route
        return self._points[self._index-1]

    @property
    def cmd(self):
        # Current command on route
        self.new = False
        return self._cmds[self._index]

    def __repr__(self):
        out = ""
        for i in range(len(self._points)):
            if i == self._index:
                out += "\033[1m{} ({:.2f}, {:.2f})\033[0m, ".format(self._cmds[i].name, self._points[i][0], self._points[i][1])
            else:
                out += "{} ({:.2f}, {:.2f}), ".format(self._cmds[i].name, self._points[i][0], self._points[i][1])
        return out


class ScannedRegion():
    all = []

    def alreadyScanned(data):
        # Check if both scanned
        SEScanned = False
        SWScanned = False
        for scan in ScannedRegion.all:
            distSE = math.sqrt(
                pow(scan.location[0] - SE_SCAN[0], 2)
                + pow(scan.location[1] - SE_SCAN[1], 2))
            if distSE < ROBOT_WIDTH:
                SEScanned = True
            distSW = math.sqrt(
                pow(scan.location[0] - SW_SCAN[0], 2)
                + pow(scan.location[1] - SW_SCAN[1], 2))
            if distSW < ROBOT_WIDTH:
                SWScanned = True
        if len(Block.allBlocks) >= NUM_BLOCKS*2:
            # 8 blocks have already been found
            return True
        if SEScanned and SWScanned and len(Block.allBlocks) < NUM_BLOCKS*2:
            # Both scanned, but not enough blocks, so must be some hidden
            logging.warning("Both regions scanned, but still missing blocks")
            ScannedRegion.all = []
            return False
        # Check if this specific scan has been done
        for scan in ScannedRegion.all:
            dist = math.sqrt(
                pow(scan.location[0] - data[0], 2)
                + pow(scan.location[1] - data[1], 2))
            if dist < ROBOT_WIDTH:
                return True
        return False

    def __init__(self, location, startHeading, endHeading):
        # Log a new region which has been scanned
        self.location = location
        self.startHeading = startHeading
        self.endHeading = endHeading
        ScannedRegion.all.append(self)

    def __repr__(self):
        return "Scanned Region: ({:.2f}, {:.2f}), {:.2f}, {:.2f};".format(
            self.location[0], self.location[1], self.startHeading, self.endHeading)


class RegionSwitch():
    # To allow the robots to switch regions
    currentRequest = None

    def __init__(self, requestBotID):
        # Used by the first robot to request the switch
        if RegionSwitch.currentRequest is not None:
            logging.error("There is already a switch request in progress")
            return
        if requestBotID == ID.REDBOT:
            self.requestBot = MyRobot.bothRobots[0]
            self.otherBot = MyRobot.bothRobots[1]
        else:
            self.requestBot = MyRobot.bothRobots[1]
            self.otherBot = MyRobot.bothRobots[0]
        self.switching = False
        # Move around the map anti-clockwise
        if checkInRectangle(EAST_REGION, self.requestBot.loc):
            self.requestBotSwitchRegion = NORTH_REGION
            self.otherBotSwitchRegion = SOUTH_REGION
            self.requestBotSwitchOpts = NE_SWITCH_OPTS
            self.requestBotMoveOpts = NW_SWITCH_OPTS
            self.otherBotSwitchOpts = SW_SWITCH_OPTS
            self.otherBotMoveOpts = SE_SWITCH_OPTS
        else:
            self.requestBotSwitchRegion = SOUTH_REGION
            self.otherBotSwitchRegion = NORTH_REGION
            self.requestBotSwitchOpts = SW_SWITCH_OPTS
            self.requestBotMoveOpts = SE_SWITCH_OPTS
            self.otherBotSwitchOpts = NE_SWITCH_OPTS
            self.otherBotMoveOpts = NW_SWITCH_OPTS
        self.requestBotSwitchLoc = None
        self.otherBotSwitchLoc = None
        # Go to switching point
        for switchLoc in self.requestBotSwitchOpts:
            route = Route([self.requestBot.loc, switchLoc], CMD.GOTO, self.requestBot.region)
            if not route.noRoute:
                self.requestBot.route = route
                self.requestBot.dispatch = True
                self.requestBotSwitchLoc = switchLoc
                break
            else:
                logging.warning("No route from {} to {}".format(self.requestBot.loc, switchLoc))
        if self.requestBot.route is None:
            # Just force it to go there, regardless of any problems
            self.requestBot.route = Route([self.requestBot.loc, self.requestBotSwitchOpts[0]], CMD.GOTO, force=True)
            self.requestBot.dispatch = True
            self.requestBotSwitchLoc = self.requestBotSwitchOpts[0]
        RegionSwitch.currentRequest = self
        logging.info("{} Requesting Switch".format(self.requestBot.name))

    def initiate(self):
        # Used by the second robot to initiate the switch
        if self.otherBotSwitchLoc is not None:
            # Check if both bots already at the switch locations
            distRequest = math.sqrt(
                pow(self.requestBot.loc[0] - self.requestBotSwitchLoc[0], 2)
                + pow(self.requestBot.loc[1] - self.requestBotSwitchLoc[1], 2))
            distOther = math.sqrt(
                pow(self.otherBot.loc[0] - self.otherBotSwitchLoc[0], 2)
                + pow(self.otherBot.loc[1] - self.otherBotSwitchLoc[1], 2))
            if distRequest < ROBOT_DIAG and distOther < ROBOT_DIAG:
                self.switch()
                return
        else:
            # Select switch location to go for the switch
            for switchLoc in self.otherBotSwitchOpts:
                route = Route([self.otherBot.loc, switchLoc], CMD.GOTO, self.otherBot.region)
                if not route.noRoute:
                    self.otherBot.route = route
                    self.otherBot.dispatch = True
                    self.otherBotSwitchLoc = switchLoc
                    break
                else:
                    logging.warning("No route from {} to {}".format(self.otherBot.loc, switchLoc))
            if self.otherBot.route is None:
                # Just force it to go there, regardless of any problems
                self.otherBot.route = Route([self.otherBot.loc, self.otherBotSwitchOpts[0]], CMD.GOTO, force=True)
                self.otherBot.dispatch = True
                self.otherBotSwitchLoc = self.otherBotSwitchOpts[0]
            logging.info("{} Initiating Switch".format(self.otherBot.name))

    def switch(self):
        # Move request bot over
        for switchLoc in self.requestBotMoveOpts:
            route = Route([self.requestBot.loc, switchLoc], CMD.GOTO)
            if not route.noRoute:
                self.requestBot.route = route
                self.requestBot.dispatch = True
                break
            else:
                # TODO: Implement something here
                logging.warning("No route from {} to {}".format(self.requestBot.loc, switchLoc))
        if self.requestBot.route is None:
            # Just force it to go there, regardless of any problems
            self.requestBot.route = Route([self.requestBot.loc, self.requestBotMoveOpts[0]], CMD.GOTO, force=True)
            self.requestBot.dispatch = True
        # Move other bot over
        for switchLoc in self.otherBotMoveOpts:
            route = Route([self.otherBot.loc, switchLoc], CMD.GOTO)
            if not route.noRoute:
                self.otherBot.route = route
                self.otherBot.dispatch = True
                break
            else:
                # TODO: Implement something here
                logging.error("No route from {} to {}".format(self.otherBot.loc, switchLoc))
        if self.otherBot.route is None:
            self.otherBot.route = Route([self.otherBot.loc, self.otherBotMoveOpts[0]], CMD.GOTO, force=True)
            self.otherBot.dispatch = True
        # Now switch the regions
        switchRegions()
        self.switching = True

    @property
    def switchComplete(self):
        # Returns if the switch has completed
        if checkInRectangle(self.requestBot.region, self.requestBot.loc)\
                and checkInRectangle(self.otherBot.region, self.otherBot.loc)\
                and self.switching:
            RegionSwitch.currentRequest = None
            return True
        else:
            return False

    @property
    def switchInProgress(self):
        # Returns if the switch is in progress
        return self.switching and not self.switchComplete

    def __repr__(self):
        return "Switching: request {}, initiate {}".format(self.requestBot, self.otherBot)


class MyRobot():
    # Representation of the robot to hold details about it
    bothRobots = []

    def crashing(timeToCrash=5, checkInterval=0.1):
        # Checks if the robots are crashing
        pointsToCheck = int(timeToCrash/checkInterval)
        bot1 = MyRobot.bothRobots[0]
        bot2 = MyRobot.bothRobots[1]
        minDist = (10, 0, 0)
        interBotDist = math.sqrt(
            pow(bot1.loc[0] - bot2.loc[0], 2)
            + pow(bot1.loc[1] - bot2.loc[1], 2))
        if interBotDist < ROBOT_DIAG*2:
            return True
        try:
            distToDestRed = math.sqrt(
                pow(bot1.loc[0] - bot1.dest[0], 2)
                + pow(bot1.loc[1] - bot1.dest[1], 2))
            timeToDestRed = distToDestRed/bot1.speed
            distToDestBlue = math.sqrt(
                pow(bot2.loc[0] - bot2.dest[0], 2)
                + pow(bot2.loc[1] - bot2.dest[1], 2))
            timeToDestBlue = distToDestBlue/bot1.speed
        except ZeroDivisionError:
            timeToDestRed = timeToCrash
            timeToDestBlue = timeToCrash

        # Check certain distance into the future to see if they collide
        for t in range(pointsToCheck):
            if t*checkInterval > timeToDestRed or t*checkInterval > timeToDestBlue:
                break
            bot1Loc = (
                bot1.speed*t*checkInterval*cosd(bot1.heading) + bot1.loc[0],
                bot1.speed*t*checkInterval*sind(bot1.heading) + bot1.loc[1]
            )
            bot2Loc = (
                bot2.speed*t*checkInterval*cosd(bot2.heading) + bot2.loc[0],
                bot2.speed*t*checkInterval*sind(bot2.heading) + bot2.loc[1]
            )
            dist = math.sqrt(
                pow(bot1Loc[0] - bot2Loc[0], 2)
                + pow(bot1Loc[1] - bot2Loc[1], 2))
            if dist < minDist[0]:
                minDist = (dist, bot1Loc, bot2Loc)
            if dist < ROBOT_DIAG*2:
                return True
        return False

    def __init__(self, isRedbot=True):
        # Create either redbot or bluebot
        self.start = True
        self.dispatch = True
        self._cmd = CMD.GOTO_AND_SPIN
        self.block = None
        self.speed = 0
        self.heading = 0
        self.route = None
        self.hasBlock = False
        self.goingHome = False
        self.stoppedTime = 0
        self.finishPrinted = False
        self.giveUpPrinted = False
        if isRedbot:
            self.data = [1.0, 1.0, 155.0, 305.0]
            self.loc = [1.0, 1.0]
            self.colour = COLOUR.RED
            self.notColour = COLOUR.BLUE
            self.id = ID.REDBOT
            self.myBlockCmd = CMD.RED_BLOCK_FOUND
            self.notMyBlockCmd = CMD.BLUE_BLOCK_FOUND
            self.baseLoc = RED_BASE_LOC
            self.homeRect = RED_HOME_RECTANGLE
            self.region = EAST_REGION
        else:
            self.data = [1.0, -1.0, 55.0, 205.0]
            if NO_BLUEBOT:
                self._cmd = CMD.GOTO
                self.data = [2.0, -2.0]
            self.loc = [1.0, -1.0]
            self.colour = COLOUR.BLUE
            self.notColour = COLOUR.RED
            self.id = ID.BLUEBOT
            self.myBlockCmd = CMD.BLUE_BLOCK_FOUND
            self.notMyBlockCmd = CMD.RED_BLOCK_FOUND
            self.baseLoc = BLUE_BASE_LOC
            self.homeRect = BLUE_HOME_RECTANGLE
            self.region = WEST_REGION
        MyRobot.bothRobots.append(self)

    @property
    def dest(self):
        # Current destination
        if self.route is None:
            return self.data[0:2]
        else:
            return self.route.point

    @property
    def cmd(self):
        # Current command
        if self.finished:
            # If finished then never let it move
            return CMD.STOP
        if self.route is None:
            return self._cmd
        else:
            return self.route.cmd

    @property
    def finished(self):
        # Returns if the robot has finished
        if Block.colourComplete(self.colour) and self.isHome:
            return True
        else:
            return False

    @property
    def isHome(self):
        # Returns if the robot is at home
        dist = math.sqrt(
            pow(self.loc[0] - self.baseLoc[0], 2)
            + pow(self.loc[1] - self.baseLoc[1], 2))
        if dist < HOME_TOLERANCE:
            return True
        else:
            return False

    @property
    def name(self):
        # Returns the name, formatted as desired
        if self.id == ID.BLUEBOT:
            txtColour = '\033[1m'
        else:
            txtColour = '\033[1m'
        return txtColour + self.id.name + '\033[0m'

    def goHome(self):
        # Send the robot home
        if not self.goingHome:
            logging.info("{} has finished, going home".format(self.name))
            self.route = Route([self.loc, self.baseLoc], CMD.GOTO)
            self.goingHome = True
            self.dispatch = True

    def setCmd(self, cmd):
        # Set the robot command
        self._cmd = cmd

    def isPoint(self, point):
        # Check if robot is occupying a certain point
        dist = math.sqrt(
            pow(self.loc[0] - point[0], 2)
            + pow(self.loc[1] - point[1], 2))
        if dist < ROBOT_DIAG:
            return True
        else:
            return False

    def __repr__(self):
        return "{}: {}, loc ({:.2f}, {:.2f}), speed {:.2f} hasBlock {}, goingHome {}, dispatch {}".format(
            self.name, self.route, self.loc[0], self.loc[1], self.speed, self.hasBlock, self.goingHome, self.dispatch
        )


def sortByFirst(e):
    # Sort by the first item
    return e[0]


def cosd(angle):
    # Cos in degrees
    return math.cos((angle/180.0)*math.pi)


def sind(angle):
    # Sin in degrees
    return math.sin((angle/180.0)*math.pi)


def checkOutsideWall(location):
    # Check if a point is outside the walls
    if abs(location[0]) > WALL_DIST or abs(location[1]) > WALL_DIST:
        return True
    else:
        return False


def checkInRectangle(rectanglePoints, location):
    # Check if point is inside a rectangle
    xMax = -100
    zMax = -100
    xMin = 100
    zMin = 100
    for point in rectanglePoints:
        if point[0] > xMax:
            xMax = point[0]
        elif point[0] < xMin:
            xMin = point[0]
        if point[1] > zMax:
            zMax = point[1]
        elif point[1] < zMin:
            zMin = point[1]
    if xMin < location[0] < xMax\
            and zMin < location[1] < zMax:
        return True
    return False


def avoidCollision(redbot, bluebot):
    # This has not been implemented
    logging.warning("Robots colliding, but avoidance not implemented - oops :(")


def switchRegions():
    # Switch the regions of the bots
    bots = MyRobot.bothRobots
    for bot in bots:
        if bot.region == EAST_REGION:
            bot.region = WEST_REGION
        else:
            bot.region = EAST_REGION


if __name__ == "__main__":
    # Initialisations
    robot = Robot()
    emitter, receiver = init_comms_devices(robot, TIME_STEP)
    keyboard = robot.getKeyboard()
    keyboard.enable(TIME_STEP)

    redbot = MyRobot(True)
    bluebot = MyRobot(False)

    keyboardWait = 0
    lastPlotTime = 0

    while robot.step(TIME_STEP) != -1:
        try:
            # Plot locations, if plotting
            if plotting:
                if lastPlotTime + 2 < simulationTime:
                    lastPlotTime = simulationTime
                    Block.plotAll()
            simulationTime = robot.getTime()
            # Get and process received packets
            packets = get_received_packets(receiver)
            for packet in packets:
                id = ID(packet[0])
                cmd = CMD(packet[1])
                data = packet[2]
                # Print packet received
                if cmd != CMD.MY_LOC:
                    logging.debug("From {} to control, {}: {}".format(packet[0].name, packet[1].name, packet[2]))
                if id == ID.REDBOT:
                    bot = redbot
                elif id == ID.BLUEBOT:
                    bot = bluebot
                else:
                    bot = None
                # Process received command
                if bot is not None:
                    clearRoute = False
                    if cmd == CMD.MY_LOC:
                        # Process bot location
                        distToDest = math.sqrt(
                            pow(bot.dest[0] - data[0], 2)
                            + pow(bot.dest[1] - data[1], 2))
                        if data[3] < 0.001 and bot.cmd == CMD.GOTO\
                                and distToDest < BLOCK_WIDTH:
                            # GOTO and arrived
                            logging.debug("{} has arrived".format(bot.name))
                            bot.dispatch = True
                            # Increment route
                            if bot.route is not None:
                                if not bot.route.getNext():
                                    logging.info("{} route finished".format(bot.name))
                                    bot.route = None
                        bot.loc = data[0:2]
                        bot.speed = data[3]
                        bot.heading = data[2]
                        if MyRobot.crashing():
                            avoidCollision(redbot, bluebot)
                        if bot.speed < STOPPED_SPEED_THRESHOLD\
                                and simulationTime > bot.stoppedTime + STOPPED_BOT_TIMEOUT\
                                and bot.dispatch is False\
                                and bot.cmd != CMD.GOTO_AND_SPIN\
                                and bot.finished is False:
                            if bot.stoppedTime == 0:
                                bot.stoppedTime = simulationTime
                            else:
                                logging.warning("{} has been stopped for too long, so dispatching".format(bot.name))
                                bot.dispatch = True
                        elif bot.speed > STOPPED_SPEED_THRESHOLD:
                            bot.stoppedTime = 0

                        # If stopped for ages at home then give up
                        if bot.speed < STOPPED_SPEED_THRESHOLD\
                                and simulationTime > bot.stoppedTime + GIVE_UP_TIMEOUT\
                                and bot.isHome\
                                and bot.finished is False:
                            if bot.stoppedTime == 0:
                                bot.stoppedTime = simulationTime
                            else:
                                bot.dispatch = False
                                bot.goingHome = True
                                bot.route = None
                                RegionSwitch.currentRequest = None
                                # Give other bot the whole map
                                if bot.id == ID.BLUEBOT:
                                    redbot.region = WHOLE_REGION
                                elif bot.id == ID.REDBOT:
                                    bluebot.region = WHOLE_REGION
                                # Clear any flags on the block it's assigned to
                                if bot.block is not None:
                                    if bot.block.colour == COLOUR.CHECKING:
                                        bot.block.colour = COLOUR.UNKNOWN
                                    bot.block = None
                                if not bot.giveUpPrinted:
                                    logging.warning("{} has been stopped for way too long, so giving up".format(bot.name))
                                    bot.giveUpPrinted = True
                        elif bot.speed > STOPPED_SPEED_THRESHOLD:
                            bot.stoppedTime = 0
                    elif cmd == CMD.SPIN_COMPLETE:
                        # Spin completed
                        if bot.cmd == CMD.GOTO_AND_SPIN:
                            logging.debug("{} has finished spinning".format(bot.name))
                            bot.dispatch = True
                        else:
                            logging.warning("{} has finished spinning, but it wasn't told to?".format(bot.name))
                            bot.dispatch = True
                        clearRoute = True
                    elif cmd == CMD.BLOCK_FOUND:
                        # Block found, still spinning so don't dispatch
                        location = [data[0], data[1]]
                        uncertainty = data[2]
                        if redbot.isPoint(location) or bluebot.isPoint(location):
                            logging.debug("Found other robot at {:.2f}, {:.2f}".format(location[0], location[1]))
                        elif checkInRectangle(RED_HOME_RECTANGLE, location) or checkInRectangle(BLUE_HOME_RECTANGLE, location):
                            logging.debug("Found block in home rectangle {:.2f}, {:.2f}".format(location[0], location[1]))
                        else:
                            newBlock = Block(location, uncertainty)
                    elif cmd == CMD.RED_BLOCK_FOUND:
                        # Block identified as red
                        location = [data[0], data[1]]
                        Block.setColour(location, COLOUR.RED)
                        bot.dispatch = True
                        clearRoute = True
                    elif cmd == CMD.BLUE_BLOCK_FOUND:
                        # Block identified as blue
                        location = [data[0], data[1]]
                        Block.setColour(location, COLOUR.BLUE)
                        bot.dispatch = True
                        clearRoute = True
                    elif cmd == CMD.BLOCK_COLLECTED:
                        # Block has been collected
                        logging.info("Sending {} home".format(bot.name))
                        location = [data[0], data[1]]
                        Block.setState(location, STATE.MOVING)
                        bot.dispatch = True
                        bot.hasBlock = True
                        clearRoute = True
                    elif cmd == CMD.BLOCK_DROPPED:
                        # Block has been dropped
                        location = [data[0], data[1]]
                        uncertainty = data[2]
                        bot.block.move(location, uncertainty)
                        bot.hasBlock = False
                        if bot.block.inRectangle(bot.homeRect):
                            bot.block.state = STATE.HOME
                            bot.dispatch = True
                            clearRoute = True
                        else:
                            # TODO: Implement handling of dropped blocks
                            logging.error("Block has been dropped")
                            bot.dispatch = True
                            clearRoute = True
                    elif cmd == CMD.NO_BLOCK:
                        # No block at location (scan error or has been accidentaly moved)
                        location = [data[0], data[1]]
                        Block.removeBlock(location)
                        bot.dispatch = True
                        clearRoute = True

                    # Clear any routing
                    if clearRoute:
                        if bot.route is not None:
                            logging.debug("{} route finished".format(bot.name))
                            bot.route = None

                    if (bot.goingHome and bot.isHome) or bot.finished:
                        # Just stop, you're done
                        if not bot.finishPrinted:
                            bot.finishPrinted = True
                            logging.warning("{} has finished".format(bot.name))
                        send_packet(emitter, bot.id, CMD.STOP, [0.0])
                        bot.dispatch = False

            # Get Keyboard Input
            if keyboardWait == 0:
                while True:
                    key = keyboard.getKey()
                    if key < 0:
                        break
                    elif key == ord('P'):
                        # Debug print
                        logging.info("All blocks {}".format(Block.allBlocks))
                        logging.info("All scanned regions {}".format(ScannedRegion.all))
                        logging.info("Both Robots: {}".format(MyRobot.bothRobots))
                        keyboardWait += 10
            else:
                keyboardWait -= 1

            # Send home immediately if time is up
            if simulationTime > RETREAT_TIME:
                for bot in MyRobot.bothRobots:
                    if not bot.finished:
                        bot.goHome()

            # Print once time is completely over
            if simulationTime > END_TIME:
                logging.warning("Time is up")

            # If one bot is finished then give the other bot the whole region
            if redbot.finished:
                bluebot.region = WHOLE_REGION
                RegionSwitch.currentRequest = None
            elif bluebot.finished:
                redbot.region = WHOLE_REGION
                RegionSwitch.currentRequest = None

            # Dispatch Robots
            for bot in (redbot, bluebot):
                # Create variable to show if switch in progress
                if RegionSwitch.currentRequest is not None:
                    if bot == RegionSwitch.currentRequest.requestBot:
                        switchInProgress = True
                    else:
                        switchInProgress = RegionSwitch.currentRequest.switchInProgress
                else:
                    switchInProgress = False
                if bot.dispatch:
                    if bot.finished:
                        # Stop if finished
                        if not bot.finishPrinted:
                            bot.finishPrinted = True
                            logging.warning("{} has finished".format(bot.name))
                        bot.setCmd(CMD.STOP)
                        send_packet(emitter, bot.id, bot.cmd, bot.data)
                        bot.dispatch = False
                    if bot.route is not None:
                        # Continue with current route
                        logging.info("{} route is {}".format(bot.name, bot.route))
                        if bot.cmd == CMD.GOTO:
                            if checkInRectangle(bot.region, bot.dest) or bot.goingHome or switchInProgress:
                                send_packet(emitter, bot.id, bot.cmd, bot.dest)
                        else:
                            send_packet(emitter, bot.id, bot.cmd, bot.data)
                        bot.dispatch = False
                    elif not switchInProgress:
                        if bot.start:
                            # Send start commands
                            bot.start = False
                            logging.debug("From control to {}, {}: {}".format(bot.name, bot.cmd.name, bot.data))
                            send_packet(emitter, bot.id, bot.cmd, bot.data)
                            bot.dispatch = False
                            if bot.cmd == CMD.GOTO_AND_SPIN:
                                newScan = ScannedRegion(bot.data[0:2], bot.data[2], bot.data[3])

                        elif bot.hasBlock:
                            # Send home
                            if checkInRectangle(bot.region, bot.baseLoc):
                                bot.route = Route([bot.loc, bot.baseLoc], CMD.RETURN_AND_DROP, bot.region)
                            else:
                                # Request switch
                                if RegionSwitch.currentRequest is not None:
                                    if RegionSwitch.currentRequest.otherBot == bot:
                                        # Initiate switch if we both want to
                                        RegionSwitch.currentRequest.initiate()
                                else:
                                    # Create new switch request
                                    newRequest = RegionSwitch(bot.id)

                        else:
                            # Decide how to dispatch the robot
                            weightBlockPairs = []
                            for i in range(len(Block.allBlocks)):
                                block = Block.allBlocks[i]
                                # Skip if not approachable
                                approachAngle = block.approachAngle(bot.loc, bot.region)
                                if approachAngle < 0:
                                    continue

                                if not block.inRectangle(bot.region):
                                    # Skip if not in bot's own region
                                    continue

                                # Try to maintain separation
                                if bot.id == ID.BLUEBOT:
                                    if redbot.block is not None:
                                        blockToRedbotBlock = math.sqrt(
                                            pow(block.location[0] - redbot.block.location[0], 2)
                                            + pow(block.location[1] - redbot.block.location[1], 2))
                                        if blockToRedbotBlock < IDEAL_BOT_SEPARATION and redbot.block.colour == COLOUR.CHECKING:
                                            logging.debug("Too close to other bot block")
                                            continue
                                else:
                                    if bluebot.block is not None:
                                        blockToBluebotBlock = math.sqrt(
                                            pow(block.location[0] - bluebot.block.location[0], 2)
                                            + pow(block.location[1] - bluebot.block.location[0], 2))
                                        if blockToBluebotBlock < IDEAL_BOT_SEPARATION and bluebot.block.colour == COLOUR.CHECKING:
                                            logging.debug("Too close to other bot block")
                                            continue

                                # Assign weights
                                weight = 0
                                logging.debug("Assigning weight to {}".format(block))
                                if block.colour == COLOUR.UNKNOWN:
                                    # Priority: Line to bot, line to base, dist
                                    if Path(bot.loc, block.approachLocation(bot.loc, bot.region)).clear:
                                        weight += WEIGHTS.MAX
                                    if Path(bot.baseLoc, block.location).clear:
                                        weight += WEIGHTS.MID
                                    dist = math.sqrt(
                                        pow(block.location[0] - bot.loc[0], 2)
                                        + pow(block.location[1] - bot.loc[1], 2))
                                    weight += DIST_FACT/dist
                                elif block.colour == bot.notColour:
                                    # Negative weight if other colour to ensure not chosen
                                    weight = WEIGHTS.NEGATIVE
                                elif block.colour == bot.colour:
                                    # Prioritise own blocks over all else
                                    weight += WEIGHTS.SUPER_MAX
                                    # Priority: Line to base, line to bot, dist
                                    if Path(bot.loc, block.approachLocation(bot.loc, bot.region)).clear:
                                        weight += WEIGHTS.MID
                                    if Path(bot.baseLoc, block.location).clear:
                                        weight += WEIGHTS.MAX
                                    dist = math.sqrt(
                                        pow(block.location[0] - bot.loc[0], 2)
                                        + pow(block.location[1] - bot.loc[1], 2))
                                    weight += DIST_FACT/dist
                                # If block is found, and not being checked, then keep track of it's weight
                                if block.state == STATE.FOUND and block.colour != COLOUR.CHECKING:
                                    weightBlockPairs.append((weight, block))

                            # Get best block
                            logging.debug("Weight block pairs: {}".format(weightBlockPairs))
                            weightBlockPairs.sort(key=sortByFirst, reverse=True)
                            if len(weightBlockPairs) > 0 and int(weightBlockPairs[0][0]) >= 0:
                                blockOfChoice = weightBlockPairs[0][1]
                                logging.info("Chosen for {}: {}".format(bot.name, blockOfChoice))
                                approachAngle = blockOfChoice.approachAngle(bot.loc, bot.region)
                                logging.debug("Approach angle: {}".format(approachAngle.name))
                                approachLocation = blockOfChoice.approachLocation(bot.loc, bot.region)
                                logging.debug("Approach location: {}".format(approachLocation))
                                if blockOfChoice.colour == bot.colour:
                                    # Collect own colour
                                    cmd = CMD.GOTO_AND_COLLECT
                                    logging.debug("Dispatch to collect")
                                else:
                                    # Check unknown colour
                                    cmd = CMD.GOTO_AND_CHECK
                                    blockOfChoice.colour = COLOUR.CHECKING
                                    logging.debug("Dispatch to check")
                                # Dispatch
                                bot.data = [blockOfChoice.x, blockOfChoice.z, blockOfChoice.uncertainty, approachAngle]
                                bot.block = blockOfChoice
                                bot.route = Route([bot.loc, approachLocation], cmd, bot.region)
                            else:
                                # Dispatch to scan, or switch regions if nothing else to do
                                distToSE = math.sqrt(
                                    pow(SE_SCAN[0] - bot.loc[0], 2)
                                    + pow(SE_SCAN[1] - bot.loc[1], 2))
                                distToSW = math.sqrt(
                                    pow(SW_SCAN[0] - bot.loc[0], 2)
                                    + pow(SW_SCAN[1] - bot.loc[1], 2))
                                if Block.colourComplete(bot.colour):
                                    # Go home, you're done
                                    bot.goHome()
                                elif (RegionSwitch.currentRequest is None) or (RegionSwitch.currentRequest.requestBot != bot):
                                    # Select which location to scan based on location
                                    if bot.id == ID.BLUEBOT:
                                        centreToOtherBot = math.sqrt(
                                            pow(redbot.dest[0] - CENTRAL_SCAN[0], 2)
                                            + pow(redbot.dest[1] - CENTRAL_SCAN[1], 2))
                                    else:
                                        centreToOtherBot = math.sqrt(
                                            pow(bluebot.dest[0] - CENTRAL_SCAN[0], 2)
                                            + pow(bluebot.dest[1] - CENTRAL_SCAN[1], 2))
                                    if False and not Route([bot.loc, CENTRAL_SCAN[0:2]], region=bot.region).noRoute and not ScannedRegion.alreadyScanned(CENTRAL_SCAN) and not centreToOtherBot < IDEAL_BOT_SEPARATION:
                                        # Currently not in use
                                        logging.info("{} has no block remaining to be dealt with, so going to centre to spin".format(bot.name))
                                        bot.data = CENTRAL_SCAN
                                        bot.route = Route([bot.loc, bot.data[0:2]], CMD.GOTO_AND_SPIN, bot.region)
                                        newScan = ScannedRegion(bot.data[0:2], bot.data[2], bot.data[3])
                                    elif not Route([bot.loc, SE_SCAN[0:2]], region=bot.region).noRoute and not ScannedRegion.alreadyScanned(SE_SCAN) and checkInRectangle(bot.region, SE_SCAN[0:2]):
                                        logging.info("{} has no block remaining to be dealt with, so going to SE to spin".format(bot.name))
                                        bot.data = SE_SCAN
                                        bot.route = Route([bot.loc, bot.data[0:2]], CMD.GOTO_AND_SPIN, bot.region)
                                        newScan = ScannedRegion(bot.data[0:2], bot.data[2], bot.data[3])
                                    elif not Route([bot.loc, SW_SCAN[0:2]], region=bot.region).noRoute and not ScannedRegion.alreadyScanned(SW_SCAN) and checkInRectangle(bot.region, SW_SCAN[0:2]):
                                        logging.info("{} has no block remaining to be dealt with, so going to SW to spin".format(bot.name))
                                        bot.data = SW_SCAN
                                        bot.route = Route([bot.loc, bot.data[0:2]], CMD.GOTO_AND_SPIN, bot.region)
                                        newScan = ScannedRegion(bot.data[0:2], bot.data[2], bot.data[3])
                                    else:
                                        # Nothing to do, so switch regions
                                        if RegionSwitch.currentRequest is not None:
                                            if RegionSwitch.currentRequest.otherBot == bot:
                                                # Initiate switch if we both want to
                                                RegionSwitch.currentRequest.initiate()
                                        else:
                                            # Create new switch request, but only permitted if in correct region
                                            if checkInRectangle(bot.region, bot.loc):
                                                newRequest = RegionSwitch(bot.id)

        except Exception as e:
            logging.error(e, exc_info=1)
