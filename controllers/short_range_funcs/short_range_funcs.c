#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/camera.h>
#include <webots/distance_sensor.h>
#include <webots/compass.h>
#include <webots/gps.h>

#include <stdio.h>
#include <math.h>

#define TIME_STEP 64

int main(int argc, char **argv) {
  /* necessary to initialize webots stuff */
  wb_robot_init();
  
  //-----------------------------------------
  // 0. INITIALISE DEVICES
  //-----------------------------------------
  
  //GPS
  WbDeviceTag gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, TIME_STEP);
  
  //Compass
  WbDeviceTag compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, TIME_STEP);
  
  //long-range distance sensor
  WbDeviceTag long_range = wb_robot_get_device("ds_long");
  wb_distance_sensor_enable(long_range, TIME_STEP);
  
  //short-range distance sensor
  WbDeviceTag short_range = wb_robot_get_device("ds_short");
  wb_distance_sensor_enable(short_range, TIME_STEP);
  
  //side camera
  WbDeviceTag camera1 = wb_robot_get_device("cs_front");
  wb_camera_enable(camera1, TIME_STEP);
  
  //motors
  WbDeviceTag wheels[2];
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }

  //grabber
  WbDeviceTag grabbers[2];
  char grabber_names[2][8] = {
    "hinge1", "hinge2"
  };
  for (int i=0; i < 2; i++) {
    grabbers[i] = wb_robot_get_device(grabber_names[i]);
    wb_motor_set_position(grabbers[i], 0);
  }
  
  //-----------------------------------------
  // INITIALISE VARIABLES
  //-----------------------------------------
  
  double wheel_radius = 0.04;
  double max_speed = 0.8;
  double linear_vel = wheel_radius * max_speed;
  
  double start = wb_robot_get_time();
  
  //INITIAL SCAN
  double rot_duration = 1.6;
  
  //APPROACH
  // double min_distance = 0.122469;
  double min_distance = 100;
  double duration1 = (min_distance - 0.07) / linear_vel;
  
  //SCAN COLOR
  int red_count = 0, blue_count = 0;
  
  //2ND APPROACH
  double distance = 0.15;
  double duration2 = distance / linear_vel;
  
  //-----------------------------------------
  // STATES (to control when different functions run)
  //-----------------------------------------
  bool bool_approach = true;
  bool bool_approach_return = false;
  
  bool bool_colour = false;
  bool bool_colour_return = false;
  
  //-----------------------------------------
  // MAIN LOOP
  //-----------------------------------------
  
  while (wb_robot_step(TIME_STEP) != -1) {
  
    double current = wb_robot_get_time();
    //printf("%f\n", current);
    
    //double left_speed = max_speed;
    //double right_speed = max_speed;
    double left_speed = 0;
    double right_speed = 0;
    
    //-----------------------------------------
    // 1. INITIAL SCAN (Rotate while scanning distance to block)
    //-----------------------------------------
    
    if (current < rot_duration) {
      left_speed = -max_speed;
      right_speed = max_speed;
    }
    //Return to initial heading
    else if (rot_duration <= current && current < 1.96 * rot_duration) {
      left_speed = max_speed;
      right_speed = -max_speed;
    }
    
    //Find distance to block from sensor readings during scan
    if (current < 1.96 * rot_duration) {
      const double volts = wb_distance_sensor_get_value(short_range);
      double distance = 0.2683*pow(volts, -1.2534);
      double distance_from_centre = distance + 0.19;
      printf("Sensor value is %f\n", distance);
      if (distance < min_distance)
        min_distance = distance;
    }
    printf("%f\n", min_distance);
    //RETURN MIN DISTANCE
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 2. APPROACH BLOCK (Travel forwards till sensor is 7cm from block)
    //-----------------------------------------
    /*
    // min_distance returned from function 1
    
    if (min_distance > 0.07) {
    //double duration1 = (min_distance - 0.07) / linear_vel;
      if (current < start + duration1) {
        left_speed = max_speed;
        right_speed = max_speed;
      }    
    }
    */
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 3. SCAN COLOUR (Use side camera sensor to scan block colour)
    //-----------------------------------------
    /*
    if (current < rot_duration) {
      left_speed = -max_speed;
      right_speed = max_speed;
    }
    //Return to initial heading
    else if (rot_duration <= current && current < 1.96 * rot_duration) {
      left_speed = max_speed;
      right_speed = -max_speed;
    }
    
    if (!bool_colour) {
      if (blue_count < 500 && red_count < 500) {
        const unsigned char *image = wb_camera_get_image(camera1);
        for (int x = 0; x < 64; x++) {
          for (int y = 40; y < 64; y++) {
            int r = wb_camera_image_get_red(image, 64, x, y);
            int b = wb_camera_image_get_blue(image, 64, x, y);
            if (r > 150) red_count++;
            if (b > 150) blue_count++;
          }
        }
      }
      else {
        colour = true;
      }
    }
    
    if (bool_colour && !bool_colour_return) {
      if (blue_count > red_count) printf("Block is blue! %d\n", blue_count);
      else printf("Block is red! %d\n", red_count);
      bool_colour_return = true;
    }
    //RETURN COLOUR
    */
    //-----------------------------------------
    
    //-----------------------------------------
    // 4. ENCLOSE (move forward to enclose the block)
    //-----------------------------------------
    /*
    //Car should move forward at designated speed for time 'duration2'
    if (current < duration2) {
      left_speed = max_speed;
      right_speed = max_speed;
    }    
    */
    //-----------------------------------------
    
    //-----------------------------------------
    // 5. GRAB (grab block by closing grabbers)
    //-----------------------------------------
    /*
    for (int i=0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_position(grabbers[i], 0.4);
      else
        wb_motor_set_position(grabbers[i], -0.4);
    }
    */
    //-----------------------------------------
    
    //If correct color, enclose block and use grabber
    //Else continue
    
    /*
    if (rot_start < current && current < rot_end) {
      left_speed = -max_speed;
      right_speed = max_speed;
    }
    else if (current > rot_end) {
      rot_start = current + duration_side;
      rot_end = rot_start + duration_turn;
    }
    */
    
    wb_motor_set_velocity(wheels[0], left_speed);
    wb_motor_set_velocity(wheels[1], right_speed);
  
  
    
    /* Process sensor data here */

    /*
     * Enter here functions to send actuator commands, like:
     * wb_motor_set_position(my_actuator, 10.0);
     */
  };

  /* Enter your cleanup code here */

  /* This is necessary to cleanup webots resources */
  wb_robot_cleanup();

  return 0;
}
