try:
    from controller import Robot
except ImportError:
    print("Failed to import from controller, fine if this isn't actually running")
import json

TIME_STEP = 4096


if __name__ == "__main__":
    robot = Robot()
    gps = robot.getDevice("gps")
    gps.enable(TIME_STEP)
    loc = None
    name = robot.getName()

    while robot.step(TIME_STEP) != -1:
        vals = gps.getValues()
        loc = [vals[0], vals[2]]
        json.dump(loc, open("{}.json".format(name), "w"))
