#ifndef DEBUGGING_H_
#define DEBUGGING_H_

#include <webots/keyboard.h>
#include <webots/robot.h>

#include <stdio.h>

// Local includes
#include "long_range.h"

void init_debugging(int time_step);
void print_info(void);
int placeholder(bool output);

#endif