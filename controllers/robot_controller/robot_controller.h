#ifndef ROBOT_CONTROLLER_H_
#define ROBOT_CONTROLLER_H_

/*
 * You may need to add include files like <webots/distance_sensor.h> or
 * <webots/motor.h>, etc.
 */
#include <webots/robot.h>

#include <stdio.h>
#include <stdlib.h>

// Local includes
#include "long_range.h"
#include "comms.h"
#include "debugging.h"
#include "short_range.h"

/*
 * You may want to add macros here.
 */
#define TIME_STEP 16
#define BLOCK_DEPOSIT_OFFSET 0.025
#define TOTAL_BLOCKS 4

// Enum for all states the robot may be in
enum states{
  STOPPED,
  WAITING,
  DRIVING_LONG,
  SPINNING,
  CHECKING,
  COLLECTING,
  DROPPING
};

#endif
