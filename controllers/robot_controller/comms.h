#ifndef COMMS_H_
#define COMMS_H_

#include <webots/robot.h>
#include <webots/emitter.h>
#include <webots/receiver.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Comms Schema
#define COMMS_CHANNEL 1
#define MAX_PACKET_LENGTH 4
#define SEND_LOC_FREQ 2.0

#define BLUEBOT_ID 0b01
#define REDBOT_ID 0b10
#define CONTROL_ID 0b11
enum commands{
  CONTINUE_CMD=0b0000,
  GOTO_AND_SPIN_CMD,
  GOTO_AND_CHECK_CMD,
  GOTO_AND_COLLECT_CMD,
  RETURN_AND_DROP_CMD,
  GOTO_CMD,
  STOP_CMD,
  CLEAR_SPACE_CMD,
  RED_BLOCK_FOUND_CMD,
  BLUE_BLOCK_FOUND_CMD,
  MY_LOC_CMD,
  SPIN_COMPLETE_CMD,
  BLOCK_FOUND_CMD,
  BLOCK_COLLECTED_CMD,
  BLOCK_DROPPED_CMD,
  NO_BLOCK_CMD
};

#define RECEIVER_MASK(byte) ((byte) & 0b11000000) >> 6
#define SENDER_MASK(byte) ((byte) & 0b00110000) >> 4
#define COMMAND_MASK(byte) ((byte) & 0b00001111)

void init_comms(int time_step);
void send_packet(char recipient, char command, double *data, int length);
char get_received_command();
double *get_received_data(double *data);

// To remove printf
void noPrintf(const char *format, ...);

#endif