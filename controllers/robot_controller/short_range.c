#include "short_range.h"

#define printf noPrintf

static WbDeviceTag gps;
static WbDeviceTag compass;
static WbDeviceTag long_range;
static WbDeviceTag short_range;
static WbDeviceTag camera1;
static WbDeviceTag camera2;
static WbDeviceTag wheels[2];
static WbDeviceTag grabbers[2];

// gps location
static double location[2];

// program start
static double start;

// resets after each run
static double min_distance = 100;
static double duration1;
static int red_count = 0;
static int blue_count = 0;
static int red_check = 0;
static int blue_check = 0;

// check state of block approach
static bool output = false;
static bool bool_scan = false;
static bool bool_scan_return = false;
static bool bool_approach = false;
static bool bool_approach_return = false;
static bool bool_colour = false;
static bool bool_colour_return = false;
static bool bool_enclose = false;
static bool bool_enclose_return = false;
static bool bool_grab = false;
static bool bool_grab_return = false;
static bool bool_block_check = false;
static bool bool_block_check_return = false;
static bool bool_deposit = false;
static bool bool_deposit_return = false;

static bool already_there = false;
static bool reset_start = false;

//initialise state for initial scan
void set_initial_scan(void) {
  min_distance = 100;
  red_count = 0;
  blue_count = 0;
  reset_start = true;
  output = false;
  bool_block_check = false;
  bool_scan = true;
  bool_scan_return = false;
  bool_approach = false;
  bool_approach_return = false;
  bool_colour = false;
  bool_colour_return = false;
  bool_enclose = false;
  bool_enclose_return = false;
  bool_grab = false;
  bool_grab_return = false;
  bool_block_check_return = false;
  bool_deposit = false;
  bool_deposit_return = false;
}

//initialise state for block collection
void set_collect(void) {
  min_distance = 100;
  reset_start = true;
  output = false;
  bool_block_check = false;
  bool_scan = true;
  bool_scan_return = false;
  bool_approach = false;
  bool_approach_return = false;
  bool_colour = false;
  bool_colour_return = false;
  bool_enclose = true;
  bool_enclose_return = false;
  bool_grab = false;
  bool_grab_return = false;
  bool_block_check_return = false;
  bool_deposit = false;
  bool_deposit_return = false;
}

//initialise state for block deposit
void set_deposit(void) {
  reset_start = true;
  output = false;
  bool_block_check = false;
  bool_scan = false;
  bool_scan_return = false;
  bool_approach = false;
  bool_approach_return = false;
  bool_colour = false;
  bool_colour_return = false;
  bool_enclose = false;
  bool_enclose_return = false;
  bool_grab = false;
  bool_grab_return = false;
  bool_block_check_return = false;
  bool_deposit = true;
  bool_deposit_return = false;
}


//function to get current gps coords of robot
int set_gps(double coords[2]) {
  if (location[0] == coords[0] && location[1] == coords[1]) {
    already_there = true;
    return 1;
  }
  else {
    already_there = false;
  }

  memcpy(location, coords, 2*sizeof(double));
  return 0;
}

//function to initialise webots sensors and actuators
void init_short_range(int time_step) {

  start = wb_robot_get_time();

  //GPS
  gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, time_step);
  
  //Compass
  compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, time_step);
  
  //long-range distance sensor
  long_range = wb_robot_get_device("ds_long");
  wb_distance_sensor_enable(long_range, time_step);
  
  //short-range distance sensor
  short_range = wb_robot_get_device("ds_short");
  wb_distance_sensor_enable(short_range, time_step);
  
  //side camera
  camera1 = wb_robot_get_device("cs_front");
  wb_camera_enable(camera1, time_step);
  
  camera2 = wb_robot_get_device("cs_block");
  wb_camera_enable(camera2, time_step);
  
  //motors
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }

  //grabber
  char grabber_names[2][8] = {
    "hinge1", "hinge2"
  };
  for (int i=0; i < 2; i++) {
    grabbers[i] = wb_robot_get_device(grabber_names[i]);
    wb_motor_set_position(grabbers[i], 0);
  }
}

//function to get colour of block with short range code
int get_colour(void) {
  //record current time
  double current = wb_robot_get_time();

  double left_speed = 0;
  double right_speed = 0;

  if (reset_start) {
    reset_start = false;
    start = current;
  }
  
  //-----------------------------------------
  // 1. INITIAL SCAN (Rotate while scanning distance to block)
  //-----------------------------------------
  if (bool_scan) {
    if (current < start + 2 * ROT_DURATION) {
      //Rotate to a certain heading to scan for block
      if (current < start + ROT_DURATION) {
        left_speed = -MAX_SPEED;
        right_speed = MAX_SPEED;
      }
      //Return to initial heading
      else if (start + ROT_DURATION <= current && current < start + 2 * ROT_DURATION) {
        left_speed = MAX_SPEED;
        right_speed = -MAX_SPEED;
      }
      
      //Find distance to block from sensor readings during scan
      const double volts = wb_distance_sensor_get_value(short_range);
      double distance = 0.2683*pow(volts, -1.2534);
      //store min distance recorded
      if (distance < min_distance)
        min_distance = distance;
    }
    else {
      bool_scan = false;
      output = true;
    }
  }
  
  if (!bool_scan && !bool_scan_return && output) {
    bool_scan_return = true;
    printf("%f\n", min_distance);
    printf("INITIAL SCAN DONE!\n");

    if (min_distance > 0.2) {
      //send command to main controller that no block is found
      send_packet(CONTROL_ID, NO_BLOCK_CMD, location, 2);
      return 1;
    }

    
    //Initialise variables for function 2
    duration1 = (min_distance - 0.07) / LINEAR_VEL;
    start = current;
    output = false;
    
    //Begin approach
    bool_approach = true;
    
  }
  
  //-----------------------------------------
  
  //-----------------------------------------
  // 2. APPROACH BLOCK (Travel forwards till sensor is 7cm from block)
  //-----------------------------------------

  // min_distance returned from function 1
  if (bool_approach) {
    //move forward a certain distance according to the min distance to get in range of colour sensor
    if (min_distance > 0.07 && current < start + duration1) {
      left_speed = MAX_SPEED;
      right_speed = MAX_SPEED;  
    }
    else {
      bool_approach = false;
      output = true;
    }
  }
  
  if (!bool_approach && !bool_approach_return && output) {
    bool_approach_return = true;
    printf("INITIAL APPROACH DONE!\n");
    //Initialise variables for function 3
    start = current;
    output = false;
    
    //Begin colour scan
    bool_colour = true;
  }
  //-----------------------------------------
  
  //-----------------------------------------
  // 3. SCAN COLOUR (Use side camera sensor to scan block colour)
  //-----------------------------------------
  
  if (bool_colour) {
    if (current < start + 0.5 + 1.4 * ROT_DURATION) {
      //Rotate to a certain heading
      if (current < start + 0.7 * ROT_DURATION) {
        left_speed = -MAX_SPEED;
        right_speed = MAX_SPEED;
      }
      //Maintain heading to scan colour of block better
      else if (start + 0.7 * ROT_DURATION <= current && current < start + 0.5 + 0.7 * ROT_DURATION ) {
        left_speed = 0;
        right_speed = 0;
      }
      //Return to initial heading
      else if (start + 0.5 + 0.7 * ROT_DURATION <= current && current < start + 0.5 + 1.4 * ROT_DURATION) {
        left_speed = MAX_SPEED;
        right_speed = -MAX_SPEED;
      }
    
      //Use the image recorded by the color to detect the color of the block
      const unsigned char *image = wb_camera_get_image(camera1);
      for (int x = 0; x < 1; x++) {
        for (int y = 0; y < 1; y++) {
          //red content of image
          int r = wb_camera_image_get_red(image, 64, x, y);
          //blue content of image
          int b = wb_camera_image_get_blue(image, 64, x, y);
          if (r > 60 && r > b) red_count++;
          if (b > 60 && b > r) blue_count++;
        }
      }
      
    }
    else {
      bool_colour = false;
      output = true;
    }
  }
  
  if (!bool_colour && !bool_colour_return && output) {
    bool_colour_return = true;
    
    printf("COLOUR SCAN DONE!\n");
    if (blue_count > red_count) {
      printf("Block is blue!\n");
      //Send command that blue block is found
      send_packet(CONTROL_ID, BLUE_BLOCK_FOUND_CMD, location, 2);
    } 
    else {
      printf("Block is red!\n");
      //Send command that red block is found
      send_packet(CONTROL_ID, RED_BLOCK_FOUND_CMD, location, 2);
    }

    //Set wheel speeds to zero
    wb_motor_set_velocity(wheels[0], 0);
    wb_motor_set_velocity(wheels[1], 0);

    return 1;
  }

  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);
  return 0;
}

//function to collect the block if color already known
int collect_block(void) {
  double current = wb_robot_get_time();
  //printf("%f\n", current);
  
  //double left_speed = MAX_SPEED;
  //double right_speed = MAX_SPEED;
  double left_speed = 0;
  double right_speed = 0;

  if (reset_start) {
    reset_start = false;
    start = current;
  }
  
  if (!already_there) {

    //-----------------------------------------
    // 1. INITIAL SCAN (Rotate while scanning distance to block)
    //-----------------------------------------
    if (bool_scan) {
      if (current < start + 2 * ROT_DURATION) {
        if (current < start + ROT_DURATION) {
          left_speed = -MAX_SPEED;
          right_speed = MAX_SPEED;
        }
        //Return to initial heading
        else if (start + ROT_DURATION <= current && current < start + 2 * ROT_DURATION) {
          left_speed = MAX_SPEED;
          right_speed = -MAX_SPEED;
        }
        
        //Find distance to block from sensor readings during scan
    
        const double volts = wb_distance_sensor_get_value(short_range);
        double distance = 0.2683*pow(volts, -1.2534);
        // printf("Sensor value is %f\n", distance);
        if (distance < min_distance)
          min_distance = distance;
      }
      else {
        bool_scan = false;
        output = true;
      }
    }
    
    if (!bool_scan && !bool_scan_return && output) {
      bool_scan_return = true;
      printf("%f\n", min_distance);
      printf("INITIAL SCAN DONE!\n");

      if (min_distance > 0.2) {
        send_packet(CONTROL_ID, NO_BLOCK_CMD, location, 2);
        return 1;
      }

      //Initialise variables for function 2
      duration1 = (min_distance - 0.07) / LINEAR_VEL;
      start = current;
      output = false;
      
      //Begin approach
      bool_approach = true;
      
    }
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 2. APPROACH BLOCK (Travel forwards till sensor is 7cm from block)
    //-----------------------------------------

    // min_distance returned from function 1
    if (bool_approach) {
      if (min_distance > 0.07 && current < start + duration1) {
        left_speed = MAX_SPEED;
        right_speed = MAX_SPEED;  
      }
      else {
        bool_approach = false;
        output = true;
      }
    }
    
    if (!bool_approach && !bool_approach_return && output) {
      bool_approach_return = true;
      printf("INITIAL APPROACH DONE!\n");
      //Initialise variables for function 3
      start = current;
      output = false;
      
      //Begin colour scan
      bool_enclose = true;
    }
    
    //-----------------------------------------
  }

  //-----------------------------------------
  // 4. ENCLOSE (move forward to enclose the block)
  //-----------------------------------------
  
  //Car should move forward at designated speed for time 'duration2'
  
  if (bool_enclose) {
    //Open up grabbers to enclose the block
    for (int i=0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_position(grabbers[i], -0.15);
      else
        wb_motor_set_position(grabbers[i], 0.15);
    }

    //Move forward to entrap the block
    if (current < start + DURATION2) {
      left_speed = MAX_SPEED;
      right_speed = MAX_SPEED;  
    }
    else {
      bool_enclose = false;
      output = true;
    }
  }
  
  if (!bool_enclose && !bool_enclose_return && output) {
    bool_enclose_return = true;
    printf("ENCLOSE DONE!\n");
    
    //Initialise variables for function 3
    start = current;
    output = false;
    
    //Begin grab
    bool_grab = true;
  }
  
  //-----------------------------------------
  
  //-----------------------------------------
  // 5. GRAB (grab block by closing grabbers)
  //-----------------------------------------
  if (bool_grab) {
    if (current < start + 0.5) {
      //Close the grabbers to grab the block
      for (int i=0; i < 2; i++) {
        if (i % 2)
          wb_motor_set_position(grabbers[i], 0.4);
        else
          wb_motor_set_position(grabbers[i], -0.4);
      }
    }
    else {
      bool_grab = false;
      output = true;
    }
  }
  
  if (!bool_grab && !bool_grab_return && output) {
    bool_grab_return = true;
    printf("BLOCK GRAB DONE!\n");
    
    if (!check_for_block()) {
      //Send command to controller if no block found
      send_packet(CONTROL_ID, NO_BLOCK_CMD, location, 2);
      return 1;
    }
    else {
      //Send command to controller if block is collected
      send_packet(CONTROL_ID, BLOCK_COLLECTED_CMD, location, 2);

      //Initialise variables for function 5
      start = current;
      output = false;
      
      //Begin block check
      bool_block_check = true;

      //Set wheel speeds to zero
      wb_motor_set_velocity(wheels[0], 0);
      wb_motor_set_velocity(wheels[1], 0);

      return 1;
    }
  }

  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);
  return 0;
}

//function to check if block is within grabbers
int check_for_block(void) {
  //Check colour content from image of single-pixel camera
  const unsigned char *image = wb_camera_get_image(camera2);
  for (int x = 0; x < 1; x++) {
    for (int y = 0; y < 1; y++) {
      int r = wb_camera_image_get_red(image, 64, x, y);
      int b = wb_camera_image_get_blue(image, 64, x, y);
      if (r > 200) red_check++;
      if (b > 200) blue_check++;
    }
  }

  if (blue_check > red_check) {

    //Set wheel speeds to zero
    wb_motor_set_velocity(wheels[0], 0);
    wb_motor_set_velocity(wheels[1], 0);

    return 1;
  } 
  else if (blue_check < red_check) {

    //Set wheel speeds to zero
    wb_motor_set_velocity(wheels[0], 0);
    wb_motor_set_velocity(wheels[1], 0);
    
    return 1;
  }
  return 0;
}

//function to deposit the block at base
int deposit_block(void) {
  double current = wb_robot_get_time();
  //printf("%f\n", current);
  
  //double left_speed = MAX_SPEED;
  //double right_speed = MAX_SPEED;
  double left_speed = 0;
  double right_speed = 0;

  if (reset_start) {
    reset_start = false;
    start = current;
  }

  if (bool_deposit) {
    //Open up grabbers
    for (int i=0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_position(grabbers[i], -0.15);
      else
        wb_motor_set_position(grabbers[i], 0.15);
    }

    //Reverse a certain distance to release block
    if (current < start + DURATION2) {
      left_speed = -MAX_SPEED;
      right_speed = -MAX_SPEED;  
    }
    else {
      bool_deposit = false;
      output = true;
    }
  }
  
  if (!bool_deposit && !bool_deposit_return && output) {
    bool_deposit_return = true;
    printf("BLOCK DEPOSIT DONE!\n");
    
    //Initialise variables for function 5
    start = current;
    output = false;

    //Set wheel speeds to zero
    wb_motor_set_velocity(wheels[0], 0);
    wb_motor_set_velocity(wheels[1], 0);

    return 1;
  }

  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);
  return 0;
}