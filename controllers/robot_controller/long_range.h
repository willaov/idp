#ifndef LONG_RANGE_H_
#define LONG_RANGE_H_

#include <webots/gps.h>
#include <webots/motor.h>
#include <webots/compass.h>
#include <webots/distance_sensor.h>
#include <webots/robot.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Local includes
#include "comms.h"

// Long Range Definitions
#define MAX_SPEED_LR 3.66
#define SPIN_SPEED 1.0
#define LONG_RANGE_MARGIN 0.01
#define KP_LR 0.5
#define KD_LR 0.04
#define LONG_RANGE_HEADING_MARGIN 0.1
#define KP_DIR 0.1
#define KD_DIR 0.02

#define SCAN_MAX_RANGE 1.4
#define SCAN_SHORT_MAX_RANGE 0.5
#define SCAN_MIN_RANGE 0.20
#define SCAN_TOLERANCE 0.09
#define SCAN_BLOCK_TOLERANCE 0.05
#define SCAN_OBSCURED_CUTOFF 0.03
#define SCAN_PERCENT_UNCERTAINTY 0.015
#define SCAN_ABSOLUTE_UNCERTAINTY 0.01
#define SCAN_BLOCK_POINTS_REQUIRED 1

#define LOOKUP_D {\
  0.15, 0.20, 0.30, 0.40,\
  0.50, 0.60, 0.70, 0.80,\
  0.90, 1.00, 1.10, 1.20,\
  1.30, 1.40, 1.50\
}
#define LOOKUP_V {\
  2.75, 2.51, 1.99, 1.52,\
  1.25, 1.04, 0.87, 0.79,\
  0.74, 0.69, 0.60, 0.55,\
  0.50, 0.47, 0.45\
}
#define LOOKUP_LEN 15

#define SHORT_LOOKUP_D {\
  0.06, 0.10, 0.15, 0.20,\
  0.25, 0.30, 0.35, 0.40,\
  0.45, 0.50, 0.60, 0.70,\
  0.80\
}
#define SHORT_LOOKUP_V {\
  3.07, 2.26, 1.62, 1.29,\
  1.09, 0.91, 0.84, 0.74,\
  0.68, 0.60, 0.52, 0.44,\
  0.41\
}
#define SHORT_LOOKUP_LEN 13

void init_long_range(int time_step);
void stop_long_range(void);
void print_navigation_info(void);
void print_sensor_info(void);
double get_heading(void);
double *get_location_info(void);
double get_wall_dist(double heading, double *location);
double get_ds_long_distance(void);
void set_destination(double dest[2]);
void set_finish_heading(double heading);
void set_spin_finish_heading(double heading);
int spin_long_range(bool has_block);
int drive_long_range(bool has_block);
int spin_scan(void);


// Physical robot parameters
#define ARRIVAL_OFFSET 0.3
#define DS_LONG_OFFSET_X 0.0795
#define DS_LONG_OFFSET_Z 0.1561
#define DS_SHORT_OFFSET_X -0.0795
#define DS_SHORT_OFFSET_Z 0.1561

#define WALL_DIST 1.19
#define HOME_X 1.0
#define HOME_Z 1.0

#endif