#ifndef SHORT_RANGE_H_
#define SHORT_RANGE_H_

#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/camera.h>
#include <webots/distance_sensor.h>
#include <webots/compass.h>
#include <webots/gps.h>

#include <stdio.h>
#include <math.h>

#include "comms.h"

#define WHEEL_RADIUS 0.0508 //constant
#define MAX_SPEED 1.2 //constant
#define LINEAR_VEL 0.06096 //constant max_speed * wheel_radius
#define ROT_DURATION 1.0 //constant
#define DISTANCE 0.15 // constant
#define DURATION2 2.47 //constant distance / linear_vel

void set_collect(void);
void set_initial_scan(void);
void init_short_range(int time_step);
int set_gps(double coords[2]);
int get_colour(void);
int collect_block(void);
int check_for_block(void);
void set_deposit(void);
int deposit_block(void);

#endif