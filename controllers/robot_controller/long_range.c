#include "long_range.h"

//#define LOGGING
#define printf noPrintf

// Static variables for the nav system
static double destination[2];
static double finish_heading;
static double spin_finish_heading;
static bool reached_destination = false;
static bool spin_finishing = false;
static WbDeviceTag gps;
static WbDeviceTag compass;
static WbDeviceTag ds_long;
static WbDeviceTag ds_short;
static WbDeviceTag wheels[2];

// For PID bearing controller
static double previous_heading = 90.0;
static double pid_interval;

// For Scanning
static double previous_ds_reading = -1.0;
static bool prev_maybe_block = false;
static bool prev_block = false;
static int block_points = 0;
static double av_block_dist = 0.0;
static double av_block_squared_dist = 0.0;
static double av_sensor_pos[2] = {0.0, 0.0};
static double block_start_heading;

// For Short Scanning
static double short_previous_ds_reading = -1.0;
static bool short_prev_maybe_block = false;
static bool short_prev_block = false;
static int short_block_points = 0;
static double short_av_block_dist = 0.0;
static double short_av_block_squared_dist = 0.0;
static double short_av_sensor_pos[2] = {0.0, 0.0};
static double short_block_start_heading;


void init_long_range(int time_step) {
  /* Initialise the long range systems */
  // Setup Wheels
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
    wb_motor_set_velocity(wheels[i], 0.0);
  }

  // Setup GPS
  gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, time_step);

  // Setup Compass
  compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, time_step);

  // Setup Long Range Distance Sensor
  ds_long = wb_robot_get_device("ds_long");
  wb_distance_sensor_enable(ds_long, time_step);

  // Setup Short Range Distance Sensor
  ds_short = wb_robot_get_device("ds_short");
  wb_distance_sensor_enable(ds_short, time_step);

  // Save time step in seconds
  pid_interval = (double)time_step/1000;
}


void stop_long_range(void) {
  /* Emergency stop the long range systems */
  for (int i=0; i < 2; i++)
    wb_motor_set_velocity(wheels[i], 0.0);
}


void print_navigation_info(void) {
  /* Print some long range nav info */
  double *location = get_location_info();
  printf("Heading: %f°; Coords: %f, %f\n", location[2], location[0], location[1]);
  printf("Current Destination: %f, %f heading: %f\n", destination[0], destination[1], finish_heading);
  free(location);
}


void print_sensor_info(void) {
  /* Print data from the sensors */
  double *location_info = get_location_info();
  double heading = location_info[2];
  double sensor_pos[2] = {
    location_info[0] + DS_LONG_OFFSET_Z*cos(heading/180.0 * M_PI) + DS_LONG_OFFSET_X*sin(heading/180.0 * M_PI),
    location_info[1] + DS_LONG_OFFSET_Z*sin(heading/180.0 * M_PI) - DS_LONG_OFFSET_X*cos(heading/180.0 * M_PI)
  };
  double wall_dist = get_wall_dist(heading, sensor_pos);
  double dist = get_ds_long_distance();
  printf("Wall dist: %f, Measured dist: %f\n", wall_dist, dist);
  free(location_info);
}


double get_heading(void) {
  /* Get the robot's current heading (x-axis north, z-axis east) */
  const double *north = wb_compass_get_values(compass);
  double rad = atan2(north[0], north[2]);
  double bearing = rad / M_PI * 180.0;
  if (bearing < 0.0)
    bearing = bearing + 360.0;
  return bearing;
}


double *get_location_info(void) {
  /* Returns a pointer to some location info (x, z, heading, speed)
   * Make sure to free the pointer once you are done with it, else it will cause a memory leak
   */
  const double *gps_values = wb_gps_get_values(gps);
  const double gps_speed = wb_gps_get_speed(gps);
  double heading = get_heading();
  double *location = malloc(sizeof(double)*4);
  location[0] = gps_values[0];
  location[1] = gps_values[2];
  location[2] = heading;
  location[3] = gps_speed;
  return location;
}


double get_ds_long_distance_webots(void) {
  /* Get long distance sensor reading: src webots */
  double ds_value = wb_distance_sensor_get_value(ds_long);
  return 0.7611*pow(ds_value, -0.9313)-0.1252;
}


double get_ds_long_distance_git(void) {
  /* Get long distance sensor reading: src https://github.com/guillaume-rico/SharpIR */
  /* Occasionally give wildly innacurate readings, so won't be using this */
  double ds_value = wb_distance_sensor_get_value(ds_long);
  return 0.60374*pow(ds_value, -1.16);
}


double get_ds_long_distance_lookup(void) {
  /* Get the long distance reading, using a lookup table */
  double dist_table[LOOKUP_LEN] = LOOKUP_D;
  double volt_table[LOOKUP_LEN] = LOOKUP_V;
  double x = wb_distance_sensor_get_value(ds_long);
  double dist = dist_table[LOOKUP_LEN-1];
  if (x > volt_table[0]) {
    dist = dist_table[0];
  } else {
    for (int i=0; i < LOOKUP_LEN-1; i++) {
      double y2 = dist_table[i+1];
      double y1 = dist_table[i];
      double x2 = volt_table[i+1];
      double x1 = volt_table[i];
      if (x1 >= x && x > x2) {
        dist = y1 + ((y2-y1)/(x2-x1))*(x-x1);
      }
    }
  }
  return dist;
}


double get_ds_long_distance(void) {
  /* Get long distance reading */
  return get_ds_long_distance_lookup();
}


double get_ds_short_distance_lookup(void) {
  /* Get short distance reading using lookup table */
  double dist_table[SHORT_LOOKUP_LEN] = SHORT_LOOKUP_D;
  double volt_table[SHORT_LOOKUP_LEN] = SHORT_LOOKUP_V;
  double x = wb_distance_sensor_get_value(ds_short);
  double dist = dist_table[SHORT_LOOKUP_LEN-1];
  if (x > volt_table[0]) {
    dist = dist_table[0];
  } else {
    for (int i=0; i < SHORT_LOOKUP_LEN-1; i++) {
      double y2 = dist_table[i+1];
      double y1 = dist_table[i];
      double x2 = volt_table[i+1];
      double x1 = volt_table[i];
      if (x1 >= x && x > x2) {
        dist = y1 + ((y2-y1)/(x2-x1))*(x-x1);
      }
    }
  }
  return dist;
}


double get_ds_short_distance(void) {
  /* Get short distance reading */
  return get_ds_short_distance_lookup();
}


double get_wall_dist(double heading, double *location) {
  /* Get the estimated distance to the wall */
  double rad = heading/180.0 * M_PI;
  double z_wall = (sin(rad) > 0.0) ? WALL_DIST : -WALL_DIST;
  double x_wall = (cos(rad) > 0.0) ? WALL_DIST : -WALL_DIST;
  double z_dist = z_wall - location[1];
  double x_dist = x_wall - location[0];
  double dist_from_x = x_dist/cos(rad);
  double dist_from_z = z_dist/sin(rad);
  return (dist_from_x < dist_from_z) ? dist_from_x : dist_from_z;
}


double diff_angles(double a, double b) {
  /* Returns the clockwise angle from a to b in degrees */
  double d = fabs(a - b);
  if (d > 360) {
    d -= 360;
  }
  double r = d > 180 ? 360 - d : d;
  double sign = (a - b >= 0 && a - b <= 180) || (a - b <=-180 && a- b>= -360) ? 1 : -1;
  r *= sign;
  return r;
}


double dist_to(double *loc1, double *loc2) {
  /* 2D Pythagoras */
  return sqrt(pow(loc1[0]-loc2[0], 2) + pow(loc1[1]-loc2[1], 2));
}


void set_destination(double dest[2]) {
  /* Set the long range destination */
  reached_destination = false;
  memcpy(destination, dest, 2*sizeof(double));
}


void set_finish_heading(double heading) {
  /* Set the long range finish heading, -1 if not required */
  finish_heading = heading;
}


void set_spin_finish_heading(double heading) {
  /* Set the spin finish heading */
  spin_finish_heading = heading;
}


void log_heading_data(double heading, double desired_heading) {
  /* Log the heading data to a file */
  FILE *f = fopen("headings.csv", "a");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f, "%f, %f,\n", heading, desired_heading);
  fclose(f);
}


void log_voltage_data(double dist1) {
  /* Log the distance voltage data to a file */
  double voltage = wb_distance_sensor_get_value(ds_long);
  FILE *f = fopen("voltage.csv", "a");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f, "%f, %f,\n", dist1, voltage);
  fclose(f);
}


void log_distance_data(double dist1, double dist2) {
  /* Log the distance data to a file */
  FILE *f = fopen("distance.csv", "a");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f, "%f, %f,\n", dist1, dist2);
  fclose(f);
}


void log_block_data(double *block_range) {
  /* Log the block data to a file */
  /* Format x_min, x_max, z_min, z_max */
  FILE *f = fopen("blocks.csv", "a");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f, "%f, %f, %f, %f,\n", block_range[0], block_range[1], block_range[2], block_range[3]);
  fclose(f);
}


int spin_long_range(bool has_block) {
  /* Spin to the set finish_heading */
  double heading = get_heading();
  if (finish_heading < 0 || fabs(diff_angles(heading, finish_heading)) < LONG_RANGE_HEADING_MARGIN) {
    wb_motor_set_velocity(wheels[0], 0.0);
    wb_motor_set_velocity(wheels[1], 0.0);
    return 1;
  }
  /* PD Controller */
  double rotation = diff_angles(finish_heading, heading);
  double heading_vel = diff_angles(heading, previous_heading);
  double vel_diff = KP_DIR * rotation - KD_DIR * heading_vel/pid_interval;
  previous_heading = heading;
  if (vel_diff > MAX_SPEED_LR) {
    vel_diff = MAX_SPEED_LR;
  } else if (vel_diff < -MAX_SPEED_LR) {
    vel_diff = -MAX_SPEED_LR;
  }

  #ifdef LOGGING
  log_heading_data(heading, finish_heading);
  #endif
  
  // Now set wheel speeds
  double left_speed = vel_diff;
  double right_speed = -vel_diff;
  if (has_block) {
    if (left_speed < 0) left_speed = 0;
    if (right_speed < 0) right_speed = 0;
  }
  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);
  return 0;
}


int drive_long_range(bool has_block) {
  /* Drive to the set destination, then spin to the set finish_heading */
  const double *gps_values = wb_gps_get_values(gps);
  double my_location[2] = {gps_values[0], gps_values[2]};
  if (dist_to(my_location, destination) < LONG_RANGE_MARGIN ||
      reached_destination) {
    /* Arrived, just need to face correct direction */
    reached_destination = true;
    if (spin_long_range(has_block)) {
      return 1;
    } else {
      return 0;
    }
  }
  /* PD Controller */
  double rad = atan2(destination[1]-my_location[1], destination[0]-my_location[0]);
  double bearing_to_dest = rad / M_PI * 180.0;
  if (bearing_to_dest < 0.0)
    bearing_to_dest = bearing_to_dest + 360.0;
  double heading = get_heading();
  double rotation = diff_angles(bearing_to_dest, heading);
  double heading_vel = diff_angles(heading, previous_heading);
  double vel_diff = KP_LR * rotation - KD_LR * heading_vel/pid_interval;
  previous_heading = heading;
  if (vel_diff < -MAX_SPEED_LR*(2-has_block))
    vel_diff = -MAX_SPEED_LR*(2-has_block);
  else if (vel_diff > MAX_SPEED_LR*(2-has_block))
    vel_diff = MAX_SPEED_LR*(2-has_block);

  // Now set wheel speeds
  double left_speed = MAX_SPEED_LR;
  double right_speed = MAX_SPEED_LR;
  if (vel_diff < 0)
    left_speed += vel_diff;
  else
    right_speed -= vel_diff;
  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);
  return 0;
}


void short_spin_scan(bool done) {
  // Now the tricky part - actually scanning for blocks - using the short range sensor
  double *location_info = get_location_info();
  double sensor_pos[2] = {
    location_info[0] + DS_SHORT_OFFSET_Z*cos(location_info[2]/180.0 * M_PI) + DS_SHORT_OFFSET_X*sin(location_info[2]/180.0 * M_PI),
    location_info[1] + DS_SHORT_OFFSET_Z*sin(location_info[2]/180.0 * M_PI) - DS_SHORT_OFFSET_X*cos(location_info[2]/180.0 * M_PI)
  };
  double ds_reading = get_ds_short_distance();
  if (short_previous_ds_reading < 0.0) short_previous_ds_reading = ds_reading;
  double wall_dist = get_wall_dist(location_info[2], sensor_pos);
  // printf("Reading: %f, Wall: %f\n", ds_reading, wall_dist);
  #ifdef LOGGING
  log_distance_data(ds_reading, wall_dist);
  log_voltage_data(wall_dist);
  #endif
  double diff = wall_dist - ds_reading;

  /* Initial checks for a block */
  bool maybe_block = ds_reading < SCAN_SHORT_MAX_RANGE && diff > SCAN_BLOCK_TOLERANCE;
  bool block = ds_reading < SCAN_SHORT_MAX_RANGE && diff > (maybe_block && short_prev_maybe_block ? SCAN_BLOCK_TOLERANCE : SCAN_TOLERANCE);
  bool block_finished = false;
  bool new_block = false;

  if (block) {
    /* Definitely a block here */
    //printf("Block, %f°, %fm", heading, ds_reading);
    if ((fabs(ds_reading-short_previous_ds_reading) > (SCAN_BLOCK_TOLERANCE + (ds_reading*SCAN_PERCENT_UNCERTAINTY*2))
    && fabs(ds_reading-short_av_block_dist) > (SCAN_BLOCK_TOLERANCE + (ds_reading*SCAN_PERCENT_UNCERTAINTY*2)))
    || (!short_prev_block && block)) {
      /* Definitely a new block */
      //printf(", New");
      block_finished = short_prev_block;
      new_block = true;
    } else if (done) {
      /* If scan has finished then just send any block that's currently being scanned */
      block_finished = true;
    }
    //printf("\n");
  } else {
    if (short_prev_block) {
      block_finished = true;
    }
  }

  if (block_finished) {
    /* Calculate block location */
    if (short_block_points > SCAN_BLOCK_POINTS_REQUIRED) {
      printf("Short Block Points %d\n", short_block_points);
      double block_width = diff_angles(location_info[2], short_block_start_heading)/180.0 * M_PI * short_av_block_dist;
      double uncertainty = fabs(SCAN_BLOCK_TOLERANCE-block_width);
      double block_end_heading = location_info[2];
      if (SCAN_BLOCK_TOLERANCE-block_width > SCAN_OBSCURED_CUTOFF) {
        if (block) {
          /* End of block obscured */
          printf("End obscured, estimating location\n");
          block_end_heading = short_block_start_heading + (SCAN_BLOCK_TOLERANCE/short_av_block_dist)/M_PI * 180.0;
        } else {
          /* Start of block obscured */
          printf("Start obscured, estimating location\n");
          short_block_start_heading = block_end_heading - (SCAN_BLOCK_TOLERANCE/short_av_block_dist)/M_PI * 180.0;
        }
      }
      /* Check StdDev of distances */
      double std_dev = sqrt(short_av_block_squared_dist - pow(short_av_block_dist, 2));
      /* Add percentage uncertainty */
      uncertainty += short_av_block_dist*SCAN_PERCENT_UNCERTAINTY;
      /* Add absolute uncertainty */
      uncertainty += SCAN_ABSOLUTE_UNCERTAINTY;
      /* Add 1/2 block width */
      short_av_block_dist += SCAN_BLOCK_TOLERANCE/2.0;
      double block_heading = diff_angles(block_end_heading, short_block_start_heading)/2 + short_block_start_heading;
      double block_location[2] = {
        short_av_sensor_pos[0] + short_av_block_dist*cos(block_heading/180.0 * M_PI),
        short_av_sensor_pos[1] + short_av_block_dist*sin(block_heading/180.0 * M_PI)
      };
      /* Send and print block location */
      double block_range[4] = {block_location[0]-uncertainty, block_location[0]+uncertainty, block_location[1]-uncertainty, block_location[1]+uncertainty};
      printf("Block Located: %.2f, %.2f; %.1f° dist %.2fm; uncertainty %.2fm; stddev %.2f\n", block_location[0], block_location[1], block_heading, short_av_block_dist, uncertainty, std_dev);
      printf("Range: %.2f - %.2f, %.2f - %.2f\n-------\n", block_range[0], block_range[1], block_range[2], block_range[3]);
      double data_to_send[3] = {block_location[0], block_location[1], uncertainty};
      send_packet(CONTROL_ID, BLOCK_FOUND_CMD, data_to_send, 3);
      log_block_data(block_range);
    } else {
      /* False block detected */
      printf("False Block\n");
    }
    /* Reset averages */
    short_av_block_dist = 0.0;
    short_av_block_squared_dist = 0.0;
    short_av_sensor_pos[0] = 0.0;
    short_av_sensor_pos[1] = 0.0;
    short_block_points = 0;
  } else if (block) {
    /* Update averages */
    short_av_block_dist = (short_av_block_dist*short_block_points + ds_reading)/(short_block_points+1);
    short_av_block_squared_dist = (short_av_block_squared_dist*short_block_points + pow(ds_reading, 2))/(short_block_points+1);
    short_av_sensor_pos[0] = (short_av_sensor_pos[0]*short_block_points + sensor_pos[0])/(short_block_points+1);
    short_av_sensor_pos[1] = (short_av_sensor_pos[1]*short_block_points + sensor_pos[1])/(short_block_points+1);
    short_block_points++;
  }

  if (new_block) {
    /* Set the start heading */
    short_block_start_heading = location_info[2];
  }

  short_previous_ds_reading = ds_reading;
  short_prev_maybe_block = maybe_block;
  short_prev_block = block;
  free(location_info);
}


int spin_scan(void) {
  /* Perform the rotational scan */
  double heading = get_heading();
  double spin_angle = spin_finish_heading - finish_heading;
  double spin_angle_remaining = spin_finish_heading - heading;
  double left_speed = SPIN_SPEED;
  double right_speed = -SPIN_SPEED;
  bool done = false;
  if (finish_heading == spin_finish_heading) {
    spin_angle = 360.0;
  }
  if (spin_angle < 0.0) spin_angle += 360.0;
  if (spin_angle_remaining < 0.0) spin_angle_remaining += 360.0;
  if (spin_angle_remaining < 1.0 && !spin_finishing) {
    // To account for small variations in angle at start of spin
    spin_finishing = false;
  } else if (spin_angle > 10.0) {
    if (spin_angle_remaining < 10.0) {
      spin_finishing = true;
    }
  } else {
    spin_finishing = true;
  }

  /* Check if spin complete */
  if (spin_finishing) {
    if (spin_angle_remaining > 180.0) {
      left_speed = 0.0;
      right_speed = 0.0;
      spin_finishing = false;
      done = true;
    }
  }
  wb_motor_set_velocity(wheels[0], left_speed);
  wb_motor_set_velocity(wheels[1], right_speed);

  // Now the tricky part - actually scanning for blocks
  double *location_info = get_location_info();
  double sensor_pos[2] = {
    location_info[0] + DS_LONG_OFFSET_Z*cos(heading/180.0 * M_PI) + DS_LONG_OFFSET_X*sin(heading/180.0 * M_PI),
    location_info[1] + DS_LONG_OFFSET_Z*sin(heading/180.0 * M_PI) - DS_LONG_OFFSET_X*cos(heading/180.0 * M_PI)
  };
  double ds_reading = get_ds_long_distance();
  if (previous_ds_reading < 0.0) previous_ds_reading = ds_reading;
  double wall_dist = get_wall_dist(heading, sensor_pos);
  // printf("Reading: %f, Wall: %f\n", ds_reading, wall_dist);
  #ifdef LOGGING
  log_distance_data(ds_reading, wall_dist);
  log_voltage_data(wall_dist);
  #endif
  double diff = wall_dist - ds_reading;

  bool maybe_block = ds_reading < SCAN_MAX_RANGE && ds_reading > SCAN_MIN_RANGE && diff > SCAN_BLOCK_TOLERANCE;
  bool block = ds_reading < SCAN_MAX_RANGE && ds_reading > SCAN_MIN_RANGE && diff > (maybe_block && prev_maybe_block ? SCAN_BLOCK_TOLERANCE : SCAN_TOLERANCE);
  bool block_finished = false;
  bool new_block = false;

  if (block) {
    /* Definitely a block here */
    //printf("Block, %f°, %fm", heading, ds_reading);
    if ((fabs(ds_reading-previous_ds_reading) > (SCAN_BLOCK_TOLERANCE + (ds_reading*SCAN_PERCENT_UNCERTAINTY*2))
    && fabs(ds_reading-av_block_dist) > (SCAN_BLOCK_TOLERANCE + (ds_reading*SCAN_PERCENT_UNCERTAINTY*2)))
    || (!prev_block && block)) {
      /* Definitely a new block */
      //printf(", New");
      block_finished = prev_block;
      new_block = true;
    } else if (done) {
      /* If scan has finished then just send any block that's currently being scanned */
      block_finished = true;
    }
    //printf("\n");
  } else {
    if (prev_block) {
      /* Block has finished */
      block_finished = true;
    }
  }

  if (block_finished) {
    /* Calculate block location */
    if (block_points > SCAN_BLOCK_POINTS_REQUIRED) {
      printf("Block Points %d\n", block_points);
      double block_width = diff_angles(heading, block_start_heading)/180.0 * M_PI * av_block_dist;
      double uncertainty = fabs(SCAN_BLOCK_TOLERANCE-block_width);
      double block_end_heading = heading;
      if (SCAN_BLOCK_TOLERANCE-block_width > SCAN_OBSCURED_CUTOFF) {
        if (block) {
          /* End of block obscured */
          printf("End obscured, estimating location\n");
          block_end_heading = block_start_heading + (SCAN_BLOCK_TOLERANCE/av_block_dist)/M_PI * 180.0;
        } else {
          /* Start of block obscured */
          printf("Start obscured, estimating location\n");
          block_start_heading = block_end_heading - (SCAN_BLOCK_TOLERANCE/av_block_dist)/M_PI * 180.0;
        }
      }
      /* Check StdDev of distances */
      double std_dev = sqrt(av_block_squared_dist - pow(av_block_dist, 2));
      /* Add percentage uncertainty */
      uncertainty += av_block_dist*SCAN_PERCENT_UNCERTAINTY;
      /* Add absolute uncertainty */
      uncertainty += SCAN_ABSOLUTE_UNCERTAINTY;
      /* Add 1/2 block width */
      av_block_dist += SCAN_BLOCK_TOLERANCE/2.0;
      double block_heading = diff_angles(block_end_heading, block_start_heading)/2 + block_start_heading;
      double block_location[2] = {
        av_sensor_pos[0] + av_block_dist*cos(block_heading/180.0 * M_PI),
        av_sensor_pos[1] + av_block_dist*sin(block_heading/180.0 * M_PI)
      };
      /* Print and send block location */
      double block_range[4] = {block_location[0]-uncertainty, block_location[0]+uncertainty, block_location[1]-uncertainty, block_location[1]+uncertainty};
      printf("Block Located: %.2f, %.2f; %.1f° dist %.2fm; uncertainty %.2fm; stddev %.2f\n", block_location[0], block_location[1], block_heading, av_block_dist, uncertainty, std_dev);
      printf("Range: %.2f - %.2f, %.2f - %.2f\n-------\n", block_range[0], block_range[1], block_range[2], block_range[3]);
      double data_to_send[3] = {block_location[0], block_location[1], uncertainty};
      send_packet(CONTROL_ID, BLOCK_FOUND_CMD, data_to_send, 3);
      log_block_data(block_range);
    } else {
      printf("False Block\n");
    }
    av_block_dist = 0.0;
    av_block_squared_dist = 0.0;
    av_sensor_pos[0] = 0.0;
    av_sensor_pos[1] = 0.0;
    block_points = 0;
  } else if (block) {
    av_block_dist = (av_block_dist*block_points + ds_reading)/(block_points+1);
    av_block_squared_dist = (av_block_squared_dist*block_points + pow(ds_reading, 2))/(block_points+1);
    av_sensor_pos[0] = (av_sensor_pos[0]*block_points + sensor_pos[0])/(block_points+1);
    av_sensor_pos[1] = (av_sensor_pos[1]*block_points + sensor_pos[1])/(block_points+1);
    block_points++;
  }

  if (new_block) {
    block_start_heading = heading;
  }

  previous_ds_reading = ds_reading;
  prev_maybe_block = maybe_block;
  prev_block = block;

  /* Also scan with short range sensor */
  short_spin_scan(done);

  // And finally cleanup
  free(location_info);
  if (done) {
    /* Send spin complete */
    double data_to_send[4] = {
      location_info[0],
      location_info[1],
      finish_heading,
      spin_finish_heading
    };
    send_packet(CONTROL_ID, SPIN_COMPLETE_CMD, data_to_send, 4);
  }
  return done;
}