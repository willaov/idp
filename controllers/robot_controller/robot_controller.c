//#define DEBUG
#include "robot_controller.h"

#define printf noPrintf

/*
 * This is the main program.
 * The arguments of the main function can be specified by the
 * "controllerArgs" field of the Robot node
 */
int main(int argc, char **argv) {
  /* necessary to initialize webots stuff */
  wb_robot_init();
  init_short_range(TIME_STEP);
  init_long_range(TIME_STEP);
  init_comms(TIME_STEP);
  #ifdef DEBUG
  init_debugging(TIME_STEP);
  #endif

  /* Declaration of variables to be used in the main loop */
  int current_state = WAITING;
  int cached_state = WAITING;
  int next_state = WAITING;
  bool has_block = false;
  int num_blocks_collected = 0;
  bool set_home_loc = true;
  double home_loc[2];

  /* Counters to get time spent in different program areas */
  int driving_counter = 0;
  int scanning_counter = 0;
  int checking_counter = 0;
  int collecting_counter = 0;
  int depositing_counter = 0;

  /* main loop
   * Perform simulation steps of TIME_STEP milliseconds
   * and leave the loop when the simulation is over
   */
  int send_loc_counter = 0;
  while (wb_robot_step(TIME_STEP) != -1) {
    // Set home location on first run
    if (set_home_loc) {
      set_home_loc = false;
      double *current_loc = get_location_info();
      memcpy(home_loc, current_loc, 2*sizeof(double));
      free(current_loc);
      printf("Home location: %f, %f\n", home_loc[0], home_loc[1]);
    }

    char cmd = get_received_command();
    double *data;
    data = malloc(sizeof(double)*MAX_PACKET_LENGTH);
    if (cmd >= 0) {
      data = get_received_data(data);
      // Update state from incoming command
      switch (cmd) {
        case GOTO_CMD:
          /* Goto location */
          set_destination(data);
          set_finish_heading(-1);
          /* Set the short range gps to something different, to prevent accidental already heres */
          set_gps(data);
          current_state = DRIVING_LONG;
          next_state = WAITING;
          break;
        case GOTO_AND_CHECK_CMD: {
          /* Goto block and check the colour */
          double heading = data[3];
          double offset = data[2] + ARRIVAL_OFFSET;
          double block_loc[2] = {data[0], data[1]};
          double destination[2] = {data[0] - cos(heading*M_PI/180.0)*offset, data[1] - sin(heading*M_PI/180.0)*offset};
          set_destination(destination);
          set_finish_heading(heading);
          set_gps(block_loc);
          set_initial_scan();
          current_state = DRIVING_LONG;
          next_state = CHECKING;
          break;
        }
        case GOTO_AND_COLLECT_CMD: {
          /* Goto block and collect it */
          double heading = data[3];
          double offset = data[2] + ARRIVAL_OFFSET;
          double block_loc[2] = {data[0], data[1]};
          if (set_gps(block_loc)) {
            /* If already here then skip long range code */
            #ifdef DEBUG
            printf("Already Here!\n");
            #endif
            set_collect();
            current_state = COLLECTING;
            next_state = WAITING;
            break;
          }
          double destination[2] = {data[0] - cos(heading*M_PI/180.0)*offset, data[1] - sin(heading*M_PI/180.0)*offset};
          set_destination(destination);
          set_finish_heading(heading);
          set_initial_scan();
          current_state = DRIVING_LONG;
          next_state = COLLECTING;
          break;
        }
        case GOTO_AND_SPIN_CMD: {
          /* Goto location and spin */
          double start_heading = data[2];
          double end_heading = data[3];
          double destination[2] = {data[0], data[1]};
          set_destination(destination);
          set_finish_heading(start_heading);
          set_spin_finish_heading(end_heading);
          current_state = DRIVING_LONG;
          next_state = SPINNING;
          break;
        }
        case RETURN_AND_DROP_CMD: {
          /* Go home and drop the block */
          set_destination(home_loc);
          set_finish_heading(-1);
          set_deposit();
          current_state = DRIVING_LONG;
          next_state = DROPPING;
          break;
        }
        case STOP_CMD:
          /* Stop everything */
          cached_state = current_state;
          current_state = STOPPED;
          break;
        case CONTINUE_CMD:
          /* Continue after stopping */
          current_state = cached_state;
          cached_state = WAITING;
          break;
      }
    }

    // Run code for the current state
    switch (current_state) {
      case DRIVING_LONG:
        /* Driving long range */
        driving_counter++;
        if (drive_long_range(false)) {
          printf("We Have Arrived!\n");
          current_state = next_state;
          next_state = WAITING;
        }
        break;
      case SPINNING:
        /* Performing a spin scan */
        scanning_counter++;
        if (spin_scan()) {
          printf("Spin Complete!\n");
          current_state = next_state;
          next_state = WAITING;
        }
        break;
      case CHECKING:
        /* Checking colour of a block */
        checking_counter++;
        if (get_colour()) {
          printf("Check Complete!\n");
          current_state = next_state;
          next_state = WAITING;
        }
        break;
      case COLLECTING:
        /* Collecting a block */
        collecting_counter++;
        if (collect_block()) {
          printf("Collected Block!\n");
          current_state = next_state;
          next_state = WAITING;
        }
        break;
      case DROPPING:
        /* Releasing a block */
        depositing_counter++;
        if (deposit_block()) {
          printf("Deposited Block!\n");
          stop_long_range();
          num_blocks_collected++;
          double deposit_data[3] = {home_loc[0], home_loc[1], 0.0};
          send_packet(CONTROL_ID, BLOCK_DROPPED_CMD, deposit_data, 3);
          // Move home location backwards
          home_loc[0] -= BLOCK_DEPOSIT_OFFSET;
          if (home_loc[1] > 0) home_loc[1] -= BLOCK_DEPOSIT_OFFSET;
          else home_loc[1] += BLOCK_DEPOSIT_OFFSET;
          current_state = next_state;
          next_state = WAITING;
        }
        break;
      case STOPPED:
        /* Stop everything */
        /*printf("Counters: driving %d, spinning %d, checking %d, collecting %d, dropping %d\n",
          driving_counter, scanning_counter, checking_counter, collecting_counter, depositing_counter);*/
        stop_long_range();
        break;
      default:
        break;
    }

    // Send location at specific intervals
    if (send_loc_counter*TIME_STEP  > 1000.0/SEND_LOC_FREQ) {
      double *location = get_location_info();
      send_packet(CONTROL_ID, MY_LOC_CMD, location, 4);
      send_loc_counter = 0;
      // And check whether it still has a block
      if (!(location[0] > 0.6
          && (location[1] > 0.6 || location[1] < -0.6))) {
        bool now_has_block = check_for_block();
        if (now_has_block != has_block) {
          printf("Block state changed from %d to %d\n", has_block, now_has_block);
          has_block = now_has_block;
        }
      }
      free(location);
    }
    send_loc_counter++;

    #ifdef DEBUG
    print_info();
    #endif
  };

  /* Enter your cleanup code here */
  free(home_loc);

  /* This is necessary to cleanup webots resources */
  wb_robot_cleanup();

  return 0;
}
