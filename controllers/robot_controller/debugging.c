#include "debugging.h"


void init_debugging(int time_step) {
  /* Initialise debugging */
  wb_keyboard_enable(time_step);
}


void print_info(void) {
  /* Print debug info when key pressed */
  while (true) {
    int key = wb_keyboard_get_key();
    if (key == -1) {
      break;
    } else {
      switch (key) {
        case 'P': {
          const char *robot_name = wb_robot_get_name();
          printf("DEBUG INFO - %s\n", robot_name);
          print_navigation_info();
          print_sensor_info();
          printf("END OF DEBUG INFO\n");
          break;
        }
        default:
          break;
      }
    }
  }
}


int placeholder(bool output) {
  /* Placeholder function */
  return output;
}