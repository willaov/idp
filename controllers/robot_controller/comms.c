#include "comms.h"

#define printf noPrintf

void noPrintf(const char *format, ...) {}

// Static variables for the comms system
static int myid;
static WbDeviceTag emitter;
static WbDeviceTag receiver;


void init_comms(int time_step) {
  /* Initialise comms devices */
  emitter = wb_robot_get_device("emitter");
  receiver = wb_robot_get_device("receiver");
  wb_receiver_enable(receiver, time_step);
  wb_emitter_set_channel(emitter, COMMS_CHANNEL);
  wb_receiver_set_channel(receiver, COMMS_CHANNEL);

  const char *robot_name = wb_robot_get_name();
  if (strncmp(robot_name, "bluebot", 7)) {
    myid = REDBOT_ID;
  } else {
    myid = BLUEBOT_ID;
  }
  printf("Comms Initialised: ID %x\n", myid);
}


void send_packet(char recipient, char command, double *data, int length) {
  /* Send a packet of data, length should be the number of doubles in the data
   */
  uint8_t header = recipient << 6 | myid << 4 | command;
  uint8_t packet[length*sizeof(double) + 1];
  packet[0] = header;
  memcpy(packet+sizeof(char), data, length*sizeof(double));
  wb_emitter_send(emitter, packet, length*sizeof(double) + 1);
}


char get_received_command() {
  /* If I've received a packet for me, get the command for it
   * Otherwise, this will return -1
   */
  while (wb_receiver_get_queue_length(receiver) > 0) {
    const uint8_t *packet = wb_receiver_get_data(receiver);
    uint8_t recipient = RECEIVER_MASK(packet[0]);
    uint8_t command = COMMAND_MASK(packet[0]);
    if (recipient == myid) {
      return command;
    } else {
      wb_receiver_next_packet(receiver);
    }
  }
  return -1;
}


double *get_received_data(double *data) {
  /* Call after getting command from packet, else this may fail
   * and will delete the command
   */
  if (wb_receiver_get_queue_length(receiver) > 0) {
    const uint8_t *packet = wb_receiver_get_data(receiver);
    uint8_t recipient = RECEIVER_MASK(packet[0]);
    if (recipient == myid) {
      int packet_length = wb_receiver_get_data_size(receiver);
      memcpy(data, packet+sizeof(char), packet_length-sizeof(char));
      wb_receiver_next_packet(receiver);
      return data;
    }
  }
  // If it gets here then this function has been called in error
  free(data);
  return NULL;
}