#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/keyboard.h>
#include <webots/camera.h>
#include <webots/distance_sensor.h>
#include <webots/compass.h>
#include <webots/gps.h>

#include <stdio.h>
#include <math.h>

#define TIME_STEP 64


// Use I (forwards) & K (backwards) to control left motor, and O & L to control right motor
// Up and down increase/decrease speed
bool colour = false;
bool colour_return = false;
// entry point of the controller
int main(int argc, char **argv) {
  // initialize the Webots API
  wb_robot_init();

  printf("%d\n", TIME_STEP); 
  // initialize devices
  // keyboard
  wb_keyboard_enable(TIME_STEP);
  
  //GPS
  WbDeviceTag gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, TIME_STEP);
  
  //Compass
  WbDeviceTag compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, TIME_STEP);
  
  //long-range distance sensor
  WbDeviceTag long_range = wb_robot_get_device("ds_long");
  wb_distance_sensor_enable(long_range, TIME_STEP);
  
  //short-range distance sensor
  WbDeviceTag short_range = wb_robot_get_device("ds_short");
  wb_distance_sensor_enable(short_range, TIME_STEP);
  
  //side camera
  WbDeviceTag camera1 = wb_robot_get_device("cs_front");
  wb_camera_enable(camera1, TIME_STEP);
  
  WbDeviceTag camera2 = wb_robot_get_device("cs_block");
  wb_camera_enable(camera2, TIME_STEP);
  
  //motors
  WbDeviceTag wheels[2];
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }
  
  //grabber
  WbDeviceTag grabbers[2];
  char grabber_names[2][8] = {
    "hinge1", "hinge2"
  };
  for (int i=0; i < 2; i++) {
    grabbers[i] = wb_robot_get_device(grabber_names[i]);
    wb_motor_set_position(grabbers[i], 0);
  }
  
  //colour sensor
  int red_count = 0, blue_count = 0;

  // feedback loop: step simulation until receiving an exit event
  double speed = 2.2;
  double grab_pos = 0.0;
  int counter = 0;
  while (wb_robot_step(TIME_STEP) != -1) {
    
    counter++;
    //printf(" %d\n", counter);

  
    //CAMERA SENSOR
    //printf("%d", counter);
    if (counter % 20 == 0) {
      const unsigned char *image = wb_camera_get_image(camera1);
      for (int x = 0; x < 1; x++) {
        for (int y = 0; y < 1; y++) {
          int r = wb_camera_image_get_red(image, 1, x, y);
          int b = wb_camera_image_get_blue(image, 1, x, y);
          printf("red=%d, blue=%d\n", r, b);
        }
      }
    }
    
    //DISTANCE SENSORS
    /*
    if (counter % 10 == 0) {
      const double volts1 = wb_distance_sensor_get_value(short_range);
      double distance1 = 0.2683 * pow(volts1, -1.2534);
      //double distance_from_centre1 = distance1 + 0.19;
      printf("Short range sensor value is %f\n", distance1);
      
      const double volts2 = wb_distance_sensor_get_value(long_range);
      double distance2 = 0.7611 * pow(volts2, -0.9313) - 0.1252;
      //double distance_from_centre2 = distance2 + 0.19;
      printf("Long range sensor value is %f\n\n", distance2);
    }
    */
    
    //GPS SENSOR
    /*
    if (counter % 10 == 0) {
      const double *coords = wb_gps_get_values(gps);
      printf("Coords: %f, %f\n", coords[0], coords[2]);
    }
    */
    
    //COMPASS SENSOR
    /*
    if (counter % 10 == 0) {
      const double *north = wb_compass_get_values(compass);
      double rad = atan2(north[0], north[2]);
      double bearing = (rad - 1.5708) / M_PI * 180.0;
      if (bearing < 0.0)
        bearing = bearing + 360.0;
      printf("Bearing: %f\n", bearing);
    }
    */
  
    
    double left_speed = 0.0;
    double right_speed = 0.0;
    while (true) {
      int key = wb_keyboard_get_key();
      if (key == -1) {
        break;
      } else {
        switch (key) {
          case 'I':
            left_speed = speed;
            break;
          case 'K':
            left_speed = -speed;
            break;
          case 'O':
            right_speed = speed;
            break;
          case 'L':
            right_speed = -speed;
            break;
          case WB_KEYBOARD_UP:
            speed += 0.1;
            printf("%f\n", speed);
            break;
          case WB_KEYBOARD_DOWN:
            speed -= 0.1;
            printf("%f\n", speed);
            break;
          case WB_KEYBOARD_LEFT:
            grab_pos -= 0.1;
            printf("Grab: %f\n", grab_pos);
            break;
          case WB_KEYBOARD_RIGHT:
            grab_pos += 0.1;
            printf("Grab: %f\n", grab_pos);
            break;
        }
      }
    }
    for (int i = 0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_velocity(wheels[i], right_speed);
      else
        wb_motor_set_velocity(wheels[i], left_speed);
    }
    for (int i=0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_position(grabbers[i], grab_pos);
      else
        wb_motor_set_position(grabbers[i], -grab_pos);
    }
  }
  // cleanup the Webots API
  wb_robot_cleanup();
  return 0; //EXIT_SUCCESS
}