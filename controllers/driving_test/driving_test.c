#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/keyboard.h>

#include <stdio.h>

#define TIME_STEP 64

// Use I (forwards) & K (backwards) to control left motor, and O & L to control right motor
// Up and down increase/decrease speed

// entry point of the controller
int main(int argc, char **argv) {
  // initialize the Webots API
  wb_robot_init();


  // initialize devices
  // keyboard
  wb_keyboard_enable(TIME_STEP);

  // motors
  WbDeviceTag wheels[2];
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }

  // grabber
  WbDeviceTag grabbers[2];
  char grabber_names[2][8] = {
    "hinge1", "hinge2"
  };
  for (int i=0; i < 2; i++) {
    grabbers[i] = wb_robot_get_device(grabber_names[i]);
    wb_motor_set_position(grabbers[i], 0);
  }

  // feedback loop: step simulation until receiving an exit event
  double speed = 6.0;
  double grab_pos = 0.0;
  while (wb_robot_step(TIME_STEP) != -1) {
    double left_speed = 0.0;
    double right_speed = 0.0;
    while (true) {
      int key = wb_keyboard_get_key();
      if (key == -1) {
        break;
      } else {
        switch (key) {
          case 'I':
            left_speed = speed;
            break;
          case 'K':
            left_speed = -speed;
            break;
          case 'O':
            right_speed = speed;
            break;
          case 'L':
            right_speed = -speed;
            break;
          case WB_KEYBOARD_UP:
            speed += 0.1;
            printf("%f\n", speed);
            break;
          case WB_KEYBOARD_DOWN:
            speed -= 0.1;
            printf("%f\n", speed);
            break;
          case WB_KEYBOARD_LEFT:
            grab_pos -= 0.1;
            printf("Grab: %f\n", grab_pos);
            break;
          case WB_KEYBOARD_RIGHT:
            grab_pos += 0.1;
            printf("Grab: %f\n", grab_pos);
            break;
        }
      }
    }
    for (int i = 0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_velocity(wheels[i], right_speed);
      else
        wb_motor_set_velocity(wheels[i], left_speed);
    }
    for (int i=0; i < 2; i++) {
      if (i % 2)
        wb_motor_set_position(grabbers[i], grab_pos);
      else
        wb_motor_set_position(grabbers[i], -grab_pos);
    }
  }
  // cleanup the Webots API
  wb_robot_cleanup();
  return 0; //EXIT_SUCCESS
}