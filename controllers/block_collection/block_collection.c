#include <webots/robot.h>
#include <webots/motor.h>
#include <webots/camera.h>
#include <webots/distance_sensor.h>
#include <webots/compass.h>
#include <webots/gps.h>

#include <stdio.h>
#include <math.h>

#define TIME_STEP 64

int main(int argc, char **argv) {
  /* necessary to initialize webots stuff */
  wb_robot_init();
  
  //-----------------------------------------
  // 0. INITIALISE DEVICES
  //-----------------------------------------
  
  //GPS
  WbDeviceTag gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, TIME_STEP);
  
  //Compass
  WbDeviceTag compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, TIME_STEP);
  
  //long-range distance sensor
  WbDeviceTag long_range = wb_robot_get_device("ds_long");
  wb_distance_sensor_enable(long_range, TIME_STEP);
  
  //short-range distance sensor
  WbDeviceTag short_range = wb_robot_get_device("ds_short");
  wb_distance_sensor_enable(short_range, TIME_STEP);
  
  //side camera
  WbDeviceTag camera1 = wb_robot_get_device("cs_front");
  wb_camera_enable(camera1, TIME_STEP);
  
  WbDeviceTag camera2 = wb_robot_get_device("cs_block");
  wb_camera_enable(camera2, TIME_STEP);
  
  //motors
  WbDeviceTag wheels[2];
  char wheels_names[2][8] = {
    "wheel1", "wheel2"
  };
  for (int i = 0; i < 2; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }

  //grabber
  WbDeviceTag grabbers[2];
  char grabber_names[2][8] = {
    "hinge1", "hinge2"
  };
  for (int i=0; i < 2; i++) {
    grabbers[i] = wb_robot_get_device(grabber_names[i]);
    wb_motor_set_position(grabbers[i], 0);
  }
  
  //-----------------------------------------
  // INITIALISE VARIABLES
  //-----------------------------------------
  
  double wheel_radius = 0.04; //constant
  double max_speed = 0.8; //constant
  double linear_vel = wheel_radius * max_speed; //constant
  
  double start = wb_robot_get_time();
  
  //INITIAL SCAN
  double rot_duration = 1.6; //constant
  
  //APPROACH
  double min_distance = 100; //reset after each run
  double duration1; //reset after each run
  
  //SCAN COLOR
  int red_count = 0, blue_count = 0; //reset after each run
  
  //2ND APPROACH
  double distance = 0.15; //constant
  double duration2 = distance / linear_vel; //constant
  
  //BLOCK CHECK
  int red_check = 0, blue_check = 0; //reset after each run
  
  //-----------------------------------------
  // STATES (to control when different functions run) //reset after each run
  //-----------------------------------------
  bool output = false;
  
  bool bool_scan = true;
  bool bool_scan_return = false;
  
  bool bool_approach = false;
  bool bool_approach_return = false;
  
  bool bool_colour = false;
  bool bool_colour_return = false;
  bool bool_isblue;
  
  bool bool_enclose = false;
  bool bool_enclose_return = false;
  
  bool bool_grab = false;
  bool bool_grab_return = false;
  
  bool bool_block_check = false;
  bool bool_block_check_return = false;
  //-----------------------------------------
  // MAIN LOOP
  //-----------------------------------------
  
  while (wb_robot_step(TIME_STEP) != -1) {
  
    double current = wb_robot_get_time();
    //printf("%f\n", current);
    
    //double left_speed = max_speed;
    //double right_speed = max_speed;
    double left_speed = 0;
    double right_speed = 0;
    
    //-----------------------------------------
    // 1. INITIAL SCAN (Rotate while scanning distance to block)
    //-----------------------------------------
    if (bool_scan) {
      if (current < 1.96 * rot_duration) {
        if (current < rot_duration) {
          left_speed = -max_speed;
          right_speed = max_speed;
        }
        //Return to initial heading
        else if (rot_duration <= current && current < 1.96 * rot_duration) {
          left_speed = max_speed;
          right_speed = -max_speed;
        }
        
        //Find distance to block from sensor readings during scan
    
        const double volts = wb_distance_sensor_get_value(short_range);
        double distance = 0.2683*pow(volts, -1.2534);
        double distance_from_centre = distance + 0.19;
        // printf("Sensor value is %f\n", distance);
        if (distance < min_distance)
          min_distance = distance;
      }
      else {
        bool_scan = false;
        output = true;
      }
    }
    
    if (!bool_scan && !bool_scan_return && output) {
      bool_scan_return = true;
      printf("%f\n", min_distance);
      printf("INITIAL SCAN DONE!\n");
 
      
      //Initialise variables for function 2
      duration1 = (min_distance - 0.07) / linear_vel;
      start = current;
      output = false;
      
      //Begin approach
      bool_approach = true;
      
    }
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 2. APPROACH BLOCK (Travel forwards till sensor is 7cm from block)
    //-----------------------------------------
 
    // min_distance returned from function 1
    if (bool_approach) {
      if (min_distance > 0.07 && current < start + duration1) {
        left_speed = max_speed;
        right_speed = max_speed;  
      }
      else {
        bool_approach = false;
        output = true;
      }
    }
    
    if (!bool_approach && !bool_approach_return && output) {
      bool_approach_return = true;
      printf("INITIAL APPROACH DONE!\n");
      //Initialise variables for function 3
      start = current;
      output = false;
      
      //Begin colour scan
      bool_colour = true;
    }
    //-----------------------------------------
    
    //-----------------------------------------
    // 3. SCAN COLOUR (Use side camera sensor to scan block colour)
    //-----------------------------------------
    
    if (bool_colour) {
      if (current < start + 1.96 * rot_duration) {
        if (current < start + rot_duration) {
          left_speed = -max_speed;
          right_speed = max_speed;
        }
        //Return to initial heading
        else if (start + rot_duration <= current && current < start + 1.96 * rot_duration) {
          left_speed = max_speed;
          right_speed = -max_speed;
        }
     

        const unsigned char *image = wb_camera_get_image(camera1);
        for (int x = 0; x < 1; x++) {
          for (int y = 0; y < 1; y++) {
            int r = wb_camera_image_get_red(image, 64, x, y);
            int b = wb_camera_image_get_blue(image, 64, x, y);
            if (r > 200) red_count++;
            if (b > 200) blue_count++;
          }
        }
        printf("redcount=%d, bluecount=%d\n", red_count, blue_count);
      }
      else {
        bool_colour = false;
        output = true;
      }
    }
    
    if (!bool_colour && !bool_colour_return && output) {
      bool_colour_return = true;
      
      printf("COLOUR SCAN DONE!\n");
      if (blue_count > red_count) {
        printf("Block is blue!\n");
        bool bool_isblue = true;
      } 
      else {
        printf("Block is red!\n");
        bool bool_isblue = false;
      }
      //RETURN COLOUR
      
      if (!bool_isblue) {
        //Initialise variables for function 4 if necessary
        start = current;
        output = false;
        
        //Begin enclose
        bool_enclose = true;
      }
    }

    
    //-----------------------------------------
    
    //-----------------------------------------
    // 4. ENCLOSE (move forward to enclose the block)
    //-----------------------------------------
    
    //Car should move forward at designated speed for time 'duration2'
    
    if (bool_enclose) {
    
      for (int i=0; i < 2; i++) {
        if (i % 2)
          wb_motor_set_position(grabbers[i], -0.3);
        else
          wb_motor_set_position(grabbers[i], 0.3);
      }
    
      if (current < start + duration2) {
        left_speed = max_speed;
        right_speed = max_speed;  
      }
      else {
        bool_enclose = false;
        output = true;
      }
    }
    
    if (!bool_enclose && !bool_enclose_return && output) {
      bool_enclose_return = true;
      printf("ENCLOSE DONE!\n");
      
      //Initialise variables for function 3
      start = current;
      output = false;
      
      //Begin grab
      bool_grab = true;
    }
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 5. GRAB (grab block by closing grabbers)
    //-----------------------------------------
    if (bool_grab) {
      if (current < start + 0.5) {
        for (int i=0; i < 2; i++) {
          if (i % 2)
            wb_motor_set_position(grabbers[i], 0.4);
          else
            wb_motor_set_position(grabbers[i], -0.4);
        }
      }
      else {
        bool_grab = false;
        output = true;
      }
    }
    
    if (!bool_grab && !bool_grab_return && output) {
      bool_grab_return = true;
      printf("BLOCK GRAB DONE!\n");
      
      //Initialise variables for function 3
      start = current;
      output = false;
      
      //Begin block check
      bool_block_check = true;
    }
    
    //-----------------------------------------
    
    //-----------------------------------------
    // 6. CHECK BLOCK ENCLOSED (use top camera to determine if block enclosed)
    //-----------------------------------------
    
    if (bool_block_check) {
      const unsigned char *image = wb_camera_get_image(camera2);
      for (int x = 0; x < 1; x++) {
        for (int y = 0; y < 1; y++) {
          int r = wb_camera_image_get_red(image, 64, x, y);
          int b = wb_camera_image_get_blue(image, 64, x, y);
          if (r > 200) red_check++;
          if (b > 200) blue_check++;
          printf("red=%d, blue=%d\n", r, b);
        }
      }
      
      bool_block_check = false;
      output = true;
    }
      
    if (!bool_block_check && !bool_block_check_return && output) {
      bool_block_check_return = true;
      printf("BLOCK CHECK DONE!\n");
      
      if (blue_check > red_check) {
        printf("Block is blue!\n");
      } 
      else if (blue_check < red_check) {
        printf("Block is red!\n");
      }
      
      //Initialise variables for function 3
      start = current;
      output = false;
    }
    
    
    
    
    
    wb_motor_set_velocity(wheels[0], left_speed);
    wb_motor_set_velocity(wheels[1], right_speed);
  
  
  };

  wb_robot_cleanup();

  return 0;
}

