import random
import os
import sys
import argparse
import json
import statistics

# argparse setup
parser = argparse.ArgumentParser()
parser.add_argument(
    "-d", "--default",
    help="use default block locations",
    action="store_true"
)
parser.add_argument(
    "-t", "--run-tests",
    help="run the set tests",
    action="store_true"
)
parser.add_argument(
    "-r", "--run-random",
    help="run random set of n tests"
)
parser.add_argument(
    "-v", "--visible",
    help="don't minimise tests while running",
    action="store_true"
)

# Set this to the world which the random world should be based on
worldFileName = "auto_test.wbt"

# Set this as the webots executable path
webots = "/Application/Webots.app/webots"

# To give non-random block locations
defaultBlockLocs = [
    (0.377, -0.9),
    (1, 0.3),
    (-1, -0.5),
    (0.1, 1.1),
    (-1.1, 1),
    (0.03, 0.72),
    (-0.85, -0.2),
    (1, 0.7)
]
random1 = [
    (0.25717871475939735, -0.22221731970511271),
    (0.5476694647400779, 0.9088094381057275),
    (-1.085479834921678, 0.048695851824933634),
    (-0.8264340294300765, -0.3820617994937302),
    (0.32993913208798675, 1.070701000931089),
    (0.9927050322590723, 0.7126923613450462),
    (-0.9248230364434797, -0.8035766037165369),
    (-0.704658670940929, 0.6736908160209032)
]
redFail = [
    (-1.113848647954895, 1.029314423772814),
    (-1.0403024135938108, -0.5703522002936082),
    (0.8450898151502693, 0.767705963777285),
    (-0.936133133170771, -0.15549744625395712),
    (0.6280074538225757, -1.0828783112354972),
    (0.7381187986795883, -0.4987714144748161),
    (-0.3825476736469666, -0.5231787535525418),
    (-0.05827309938638625, 0.9166633358424296)
]
redFail2 = [
    (0.09052075046254116, -0.9380290349631636),
    (-0.19772243543983936, -0.9533728437071074),
    (0.7645243876470698, 1.0589548543407545),
    (1.100237592356763, -0.4940864637339003),
    (0.443089460783036, 0.5927870431041036),
    (0.9155823042412541, -0.2455622234974717),
    (-0.718766425285452, -0.6001836962258305),
    (0.008694250244222212, 0.6780294688813255)
]
redFail3 = [
    (-0.1077289464664648, 0.3130082620138124),
    (-0.5957889332566304, -0.258019489364551),
    (-0.08793442180059108, -0.3585285645892209),
    (-0.9679048898848585, 0.9377635686388368),
    (-0.010171105549671156, -0.7382102055409185),
    (0.5543668931440491, 1.042773783509972),
    (0.11890281378008472, 0.9113214900318201),
    (0.5489833123949983, 0.5722904205402259)
]
redPoor = [
    (-0.9035371159467624, 1.1227717370473593),
    (-0.3473340710599152, -0.04364498455593613),
    (0.57975632357618, -0.34577034273691887),
    (-0.0005332018652377535, -0.9481621184713942),
    (1.146568316250478, -0.5532562334882717),
    (-0.750232485179211, -0.4481522211769092),
    (-1.0783273647505656, -0.9901116086891478),
    (-0.5507686771134885, 1.1122286136640813)
]
bluePoor = [
    (-0.0820737483135432, 0.6594224627734744),
    (0.38576228068185614, 0.8528000112151619),
    (-0.6219840609465048, -0.5685388079327356),
    (0.15372260669593096, -0.20983498052363025),
    (-0.6792913729026757, 0.0567650759110776),
    (0.44753305589478853, 0.2109950197540964),
    (0.5018989144248707, -0.5322888534134689),
    (-0.4703874223374742, 0.44945351504856657)
]

# tests = [defaultBlockLocs, random1]
tests = [redFail, redFail2, redFail3, redPoor, bluePoor]

BLOCK_WIDTH = 0.05
RED_HOME_RECTANGLE = [
    (0.8, 0.8),
    (0.8, 1.2),
    (1.2, 1.2),
    (1.2, 0.8)
]
BLUE_HOME_RECTANGLE = [
    (0.8, -0.8),
    (0.8, -1.2),
    (1.2, -1.2),
    (1.2, -0.8)
]

blockFileNames = ["R1", "R2", "R3", "R4", "B1", "B2", "B3", "B4"]
blockFiles = []
for i in range(len(blockFileNames)):
    blockFiles.append(os.path.join("controllers", "auto_block", "{}.json".format(blockFileNames[i])))
baseWorld = os.path.join("worlds", worldFileName)
randomWorld = os.path.join("worlds", "random_world.wbt")


def randLoc():
    return (random.uniform(-1.165, 1.165), random.uniform(-1.165, 1.165))


def makeValid(randomBlockLocs):
    valid = False
    while valid is False:
        valid = True
        for i in range(len(randomBlockLocs)):
            # Check for home
            if randomBlockLocs[i][0] > 0.775:
                if randomBlockLocs[i][1] > 0.775 or randomBlockLocs[i][1] < -0.775:
                    valid = False
                    randomBlockLocs[i] = randLoc()
            # Check for intersecting
            for j in range(i):
                x_dist = abs(randomBlockLocs[j][0]-randomBlockLocs[i][0])
                z_dist = abs(randomBlockLocs[j][1]-randomBlockLocs[i][1])
                if x_dist <= 0.05 and z_dist <= 0.05:
                    randomBlockLocs[i] = randLoc()


def inRectangle(blockLoc, rectanglePoints):
    # Check if block is inside a rectangle
    xMax = -100
    zMax = -100
    xMin = 100
    zMin = 100
    for point in rectanglePoints:
        if point[0] > xMax:
            xMax = point[0]
        elif point[0] < xMin:
            xMin = point[0]
        if point[1] > zMax:
            zMax = point[1]
        elif point[1] < zMin:
            zMin = point[1]
    if xMin - BLOCK_WIDTH/2 < blockLoc[0] < xMax + BLOCK_WIDTH/2\
            and zMin - BLOCK_WIDTH/2 < blockLoc[1] < zMax + BLOCK_WIDTH/2:
        return True
    return False


if __name__ == "__main__":
    # Analysis
    failedBlockNum = []

    args = parser.parse_args()
    run = 0
    numRuns = 1
    while run < numRuns:
        # Read base world file, and get lines to edit
        fr = open(baseWorld, "r")
        data = fr.readlines()
        blockLines = []
        for lineNum in range(len(data)):
            if "DEF Block_" in data[lineNum]:
                blockLines.append(lineNum)

        # Parse Arguments and setup block locations
        randomBlockLocs = []
        if args.default:
            randomBlockLocs = defaultBlockLocs
        elif args.run_tests:
            randomBlockLocs = tests[run]
            numRuns = len(tests)
        else:
            if args.run_random:
                numRuns = int(args.run_random)
            for i in range(len(blockLines)):
                randomBlockLocs.append(randLoc())
            makeValid(randomBlockLocs)

        # Write new file with new block locations
        for i in range(len(blockLines)):
            lineNum = blockLines[i]
            data[lineNum+1] = "  translation {} 0.05 {}\n".format(
                randomBlockLocs[i][0], randomBlockLocs[i][1])

        fw = open(randomWorld, "w")
        fw.writelines(data)

        fr.close()
        fw.close()

        for loc in randomBlockLocs:
            print(loc)

        if os.name == "posix":
            cmdArgs = ""
            if args.visible:
                cmdArgs = " --mode=realtime"
            print("Running world automatically")
            if sys.platform == "darwin":
                os.system("./auto_test.sh /Applications/Webots.app/webots{}".format(cmdArgs))
            else:
                print("Linux is not yet tested")
                os.system("./auto_test.sh webots{}".format(cmdArgs))
        else:
            print("You're using Windows, so can't run automatically")
            input(
                "Ready to analyse blocks? Press enter once you have run the world.\n" +
                "To run the world from terminal type webots --mode=fast --no-rendering " +
                "--minimize --batch --stdout --stderr " + randomWorld)

        # Check all block locations
        failed = 0
        for i in range(len(blockFiles)):
            blockLoc = json.load(open(blockFiles[i], "r"))
            if "R" in blockFileNames[i]:
                print("Red", blockLoc)
                if not inRectangle(blockLoc, RED_HOME_RECTANGLE):
                    print("Failed")
                    failed += 1
            elif "B" in blockFileNames[i]:
                print("Blue", blockLoc)
                if not inRectangle(blockLoc, BLUE_HOME_RECTANGLE):
                    print("Failed")
                    failed += 1

        failedBlockNum.append(failed)

        # Increment the run counter
        run += 1

    # Print analysis
    if len(failedBlockNum) > 1:
        mean = statistics.mean(failedBlockNum)
        stdDev = statistics.stdev(failedBlockNum)
        print("Failed to find and retrieve μ={:.2f}, σ={:.2f}".format(mean, stdDev))
        print("Successes: {} {:.0f}%".format(failedBlockNum.count(0), failedBlockNum.count(0)*100/len(failedBlockNum)))
        print("1 Failed: {} {:.0f}%".format(failedBlockNum.count(1), failedBlockNum.count(1)*100/len(failedBlockNum)))
        print("2 Failed: {} {:.0f}%".format(failedBlockNum.count(2), failedBlockNum.count(2)*100/len(failedBlockNum)))
        print("3 Failed: {} {:.0f}%".format(failedBlockNum.count(3), failedBlockNum.count(3)*100/len(failedBlockNum)))
        print("4 Failed: {} {:.0f}%".format(failedBlockNum.count(4), failedBlockNum.count(4)*100/len(failedBlockNum)))
        print("5 Failed: {} {:.0f}%".format(failedBlockNum.count(5), failedBlockNum.count(5)*100/len(failedBlockNum)))
        print("6 Failed: {} {:.0f}%".format(failedBlockNum.count(6), failedBlockNum.count(6)*100/len(failedBlockNum)))
        print("7 Failed: {} {:.0f}%".format(failedBlockNum.count(7), failedBlockNum.count(7)*100/len(failedBlockNum)))
        print("All Failed: {} {:.0f}%".format(failedBlockNum.count(8), failedBlockNum.count(8)*100/len(failedBlockNum)))
    else:
        print("Failed to find and retrieve {}".format(failedBlockNum[0]))
